jQuery(document).ready(function() {
	jQuery('table.nmedamam-table a.delete').bind('click', function(objEvent) {
		if (confirm('Are you sure you want to delete this ' + jQuery(this).attr('data-type') + '?\n\nPress [OK] to continue, or [CANCEL] to abort.')) {
			return true;
		} else {
			return false;
		}
	});
});