// initialization
jQuery(document).ready(function() {
	jQuery('#nmeda_entrant_mediatype').bind('change', NMEDA_MAM.fn.entry.media.set_type);
	jQuery('#nmeda_entrant_video_url').bind('change', NMEDA_MAM.fn.entry.media.get_video_details);
});

var NMEDA_MAM = {
	p: {
	}, 
	fn: {
		entry: {
			media: {
				get_youtube_video_id: function(strVideoURL) {
					var strVideoID			= '';
					var objVideoRegExp		= /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
					var arrMatches			= strVideoURL.match(objVideoRegExp);
					
					if ((arrMatches) && (arrMatches[7].length == 11)) {
						strVideoID				= arrMatches[7];
					}
					
					return strVideoID;
				}, 

				set_type: function() {
					var objMediaPhoto			= jQuery('#nmeda-entrant-media-photo');
					var objMediaVideo			= jQuery('#nmeda-entrant-media-video');
					var objMediaPreview			= jQuery('#nmeda-entrant-media-video-preview');
					var strMediaVideoURL		= jQuery('#nmeda_entrant_video_url').val();
					
					jQuery(objMediaPhoto).hide();
					jQuery(objMediaVideo).hide();
					jQuery(objMediaPreview).hide();
					
					switch (parseInt(jQuery(this).val())) {
						case 1 : 
							// photo
							jQuery(objMediaPhoto).show();
							break;
							
						case 2 : 
							// video
							jQuery(objMediaVideo).show();
							
							if (strMediaVideoURL.length) {
								jQuery(objMediaPreview).show();
							}
							break;
					}
				}, 

				get_video_details: function() {
					var strVideoURL			= jQuery('#nmeda_entrant_video_url').val();
					var strVideoID;
					
					if (strVideoURL.length) {
						strVideoID				= NMEDA_MAM.fn.entry.media.get_youtube_video_id(strVideoURL);
						
						if (strVideoID.length) {
							jQuery('#nmeda-entrant-media-video-preview').show();
							jQuery('#nmeda-entrant-media-video-preview>dd').html('<p>Obtaining video details from YouTube, please wait...</p>').show();
							
							jQuery.post(	'/wp-admin/admin-ajax.php?action=nmeda_get_youtube_details', 
											{
												id: strVideoID
											}, 
											function(objData) {
												if (objData.success === true) {
													if ((objData.data.data.id.length) && (objData.data.data.thumbnail.sqDefault.length) && (objData.data.data.duration > 0)) {
														if (objData.data.data.duration > 120) {
															alert('That video is longer than 2 minutes. Videos are limited to 2 minutes in length. Please try again with a shorter length video.');
															jQuery('#nmeda-entrant-media-video-preview>dd').html('');
															jQuery('#nmeda-entrant-media-video-preview').hide();
														} else {
															var strVideoPlayerURL			= objData.data.data.player.default;
															var strVideoID					= objData.data.data.id;
															var strVideoTitle				= objData.data.data.title;
															var strVideoThumbnailSmall		= objData.data.data.thumbnail.sqDefault;
															var strVideoThumbnailLarge		= objData.data.data.thumbnail.hqDefault;
															var intVideoDurationMins		= Math.floor(objData.data.data.duration / 60);
															var intVideoDurationSeconds		= ((intVideoDurationMins > 0) ? (objData.data.data.duration % 60) : objData.data.data.duration);
															
															jQuery('#nmeda_entrant_video_url').val(strVideoPlayerURL);
															jQuery('#nmeda_entrant_video_thumb_small').val(strVideoThumbnailSmall);
															jQuery('#nmeda_entrant_video_thumb_large').val(strVideoThumbnailLarge);
															jQuery('#nmeda-entrant-media-video-preview>dd').html('<iframe id="ytplayer" type="text/html" width="320" height="240" src="http://www.youtube.com/embed/' + strVideoID + '?theme=light" frameborder="0"></iframe>');
														}
													} else {
														alert('An error occurred while attempting to process that video. Please try again.');
														jQuery('#nmeda-entrant-media-video-preview>dd').html('');
														jQuery('#nmeda-entrant-media-video-preview').hide();
													}
												} else {
													alert('An error occurred while attempting to process that video. Please try again.');
													jQuery('#nmeda-entrant-media-video-preview>dd').html('');
													jQuery('#nmeda-entrant-media-video-preview').hide();
												}
											}, 
											"json");
						} else {
							alert('That video does not appear to be a valid YouTube video URL. Only videos on YouTube are allowed.');
							jQuery('#nmeda-entrant-media-video-preview>dd').html('');
							jQuery('#nmeda-entrant-media-video-preview').hide();
						}
					}
				}, 
			}
		}
	}
};
