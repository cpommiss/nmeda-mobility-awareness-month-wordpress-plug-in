<?php
/*
Plugin Name: NMEDA - Mobility Awareness Month
Plugin URI: http://www.mobilityawarenessmonth.com/
Description: Implements Create/Read/Update/Delete functionality for mobility awareness month contest entrants.
Version: 1.0
*/

require_once ABSPATH . 'wp-admin/includes/image.php';
  
class NMEDA_MAM {
	const VOTING_TIMEZONE			= -7;	// or -8 depending on DST status
	
	const REALTIME_VOTE_COUNTS		= true;
	
	const ENTRANT_POSTTYPE_SLUG		= 'entrant';
	const ENTRANT_ADMIN_EMAIL		= 'casey.kope@evokad.com';
	const ENTRANT_STATUS_NOT_FOUND	= 0;
	const ENTRANT_STATUS_FOUND		= 1;
	const ENTRANT_STATUS_ACCEPTED	= 2;
	const ENTRANT_MEDIA_TYPE_PHOTO	= 1;
	const ENTRANT_MEDIA_TYPE_VIDEO	= 2;
	
	const ENTRANT_META_VOTES		= '_nmam_votes';
	const ENTRANT_META_VOTES_BONUS	= '_nmam_votes_bonus';
	const ENTRANT_META_NOTIFY_PUB	= '_nmam_publish_notify';
	const ENTRANT_META_HERO_FIRST	= '_nmam_hero_firstname';
	const ENTRANT_META_HERO_LAST	= '_nmam_hero_lastname';
	const ENTRANT_META_HERO_CITY	= '_nmam_hero_city';
	const ENTRANT_META_HERO_STATE	= '_nmam_hero_state';
	
	const EXTRAVOTE_POSTTYPE_SLUG	= 'extravote';
	const EXTRAVOTE_FIELD_ANSWER1	= '_nmam_extravote_answer_1';
	const EXTRAVOTE_FIELD_ANSWER2	= '_nmam_extravote_answer_2';
	const EXTRAVOTE_FIELD_ANSWER3	= '_nmam_extravote_answer_3';
	const EXTRAVOTE_FIELD_ANSWER4	= '_nmam_extravote_answer_4';
	const EXTRAVOTE_FIELD_CORRECT	= '_nmam_extravote_correct_answer';
	const EXTRAVOTE_FIELD_DETAILS	= '_nmam_extravote_details';
	
	const NONCE_VOTING_TOKEN		= '_wp_nmam_entrant_votetoken';
	
	const EXTRAVOTE_STATUS_ANY		= 0;
	const EXTRAVOTE_STATUS_EARNED	= 1;
	const EXTRAVOTE_STATUS_SPENT	= 2;
	
	const CAPTCHA_MATH_OPERATOR		= '_sfcampus_math_captcha_operator';
	const CAPTCHA_MATH_RESULT		= '_sfcampus_math_captcha_result';
	const CAPTCHA_MATH_OPERATOR_ADD	= 1;
	const CAPTCHA_MATH_OPERATOR_SUB	= 2;
	
	private $db;
	private $datasets				= array();
	
	/*
	==========================================================================
	CONSTRUCTOR
	==========================================================================	
	*/
	
	/**
	 * Initializes the plugin by setting localization, filters, and administration functions.
	 */
	function __construct() {
		global $wpdb;
		
		@session_start();
		
		$this->db			= &$wpdb;
		
		load_plugin_textdomain('nmeda-mam-locale', false, dirname(plugin_basename(__FILE__)) . '/lang');
		
		// register initialization action
		add_action('init', array($this, 'initialize'));
		add_action('admin_init', array($this, 'admin_initialize'));
		add_action('admin_menu', array($this, 'admin_menu'));
		add_action('admin_head', array($this, 'admin_header'));
		add_action('save_post', array($this, 'admin_save'));
		
		// Register admin styles and scripts
		add_action('admin_print_styles', array($this, 'register_admin_styles'));
		add_action('admin_enqueue_scripts', array($this, 'register_admin_scripts'));

		register_activation_hook(__FILE__, array($this, 'activate'));
		register_deactivation_hook(__FILE__, array($this, 'deactivate'));

		// Set WordPress e-mail content type to HTML
		add_filter('wp_mail_content_type', create_function('', 'return "text/html";'));

		// AJAX: obtain details of a youtube video link
		add_action( 'wp_ajax_nmeda_get_youtube_details', array( $this, 'get_youtube_details' ) );
		add_action( 'wp_ajax_nopriv_nmeda_get_youtube_details', array( $this, 'get_youtube_details' ) );

		// AJAX: handle file upload
		add_action( 'wp_ajax_nmeda_handle_file_upload', array( $this, 'handle_file_upload' ) );
		add_action( 'wp_ajax_nopriv_nmeda_handle_file_upload', array( $this, 'handle_file_upload' ) );

		// AJAX: get extra vote question
		add_action( 'wp_ajax_nmeda_get_extravote_question', array( $this, 'get_extravote_question' ) );
		add_action( 'wp_ajax_nopriv_nmeda_get_extravote_question', array( $this, 'get_extravote_question' ) );

		// AJAX: verify extra vote question
		add_action( 'wp_ajax_nmeda_verify_extravote_question', array( $this, 'verify_extravote_question' ) );
		add_action( 'wp_ajax_nopriv_nmeda_verify_extravote_question', array( $this, 'verify_extravote_question' ) );
	}
	
	/**
	 * Fired when the plugin is activated.
	 *
	 * @params	$network_wide	True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog 
	 */
	public function activate($network_wide) {
		
	}
	
	/**
	 * Fired when the plugin is deactivated.
	 *
	 * @params	$network_wide	True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog 
	 */
	public function deactivate($network_wide) {
		
	}
	
	/**
	 * Registers and enqueues admin-specific styles.
	 */
	public function register_admin_styles() {
		wp_enqueue_style( 'nmeda-mam-admin-styles-base', plugins_url( 'nmeda-mobility-awareness-month/css/admin.css' ) );
		wp_enqueue_style( 'nmeda-mam-admin-styles-jqui', plugins_url( 'nmeda-mobility-awareness-month/css/vendor/jquery-ui-1.9.2.custom.min.css' ) );
	}

	/**
	 * Registers and enqueues admin-specific JavaScript.
	 */	
	public function register_admin_scripts() {
		wp_enqueue_script( 'suggest' );
		wp_enqueue_script( 'nmeda-mam-admin-script-base', plugins_url( 'nmeda-mobility-awareness-month/js/admin.js' ) );
		wp_enqueue_script( 'nmeda-mam-admin-script-base-init', plugins_url( 'nmeda-mobility-awareness-month/js/admin.init.js' ) );
		wp_enqueue_script( 'nmeda-mam-admin-script-jqui', plugins_url( 'nmeda-mobility-awareness-month/js/vendor/jquery-ui-1.9.2.custom.min.js' ) );
	}
	
	/**
	 * Registers and enqueues plugin-specific styles.
	 */
	public function register_plugin_styles() {
		wp_enqueue_style( 'nmeda-mam-plugin-styles', plugins_url( 'nmeda-mobility-awareness-month/css/display.css' ) );
	
	}
	
	/**
	 * Registers and enqueues plugin-specific scripts.
	 */
	public function register_plugin_scripts() {
		wp_enqueue_script( 'nmeda-mam-plugin-script', plugins_url( 'nmeda-mobility-awareness-month/js/display.js' ) );
	}
	
	/*	
	==========================================================================
	INITIALIZATION METHODS
	==========================================================================	
	*/
	
	/**
	 * Runs when the page is initialized.
	 */
	public function initialize() {
		// register the 'entrant' type
		$arrEntrantsLabels			=	array(
											'name'					=> __('Contest Entrants'), 
											'singular_name'			=> __('Contest Entrant'), 
											'add_new'				=> __('Add New'), 
											'add_new_item'			=> __('Add New Contest Entrant'), 
											'edit_item'				=> __('Edit Contest Entrant'), 
											'new_item'				=> __('New Contest Entrant'), 
											'view_item'				=> __('View Contest Entrant'), 
											'search_items'			=> __('Search Contest Entrants'), 
											'not_found'				=> __('Nothing found'), 
											'not_found_in_trash'	=> __('Nothing found in Trash'), 
											'parent_item_colon'		=> ''
									);
		$arrEntrantsSettings		=	array(
											'labels'				=> $arrEntrantsLabels, 
											'public'				=> true, 
											'publicly_queryable'	=> true, 
											'show_ui'				=> true, 
											'query_var'				=> true, 
											'menu_icon'				=> plugins_url('images/icon.png', __FILE__), 
											'rewrite'				=> array('slug' => self::ENTRANT_POSTTYPE_SLUG), 
											'capability_type'		=> 'page', 
											'hierarchial'			=> false, 
											'menu_position'			=> NULL, 
											'supports'				=> array('title', 'editor', 'thumbnail')
										);
		register_post_type(self::ENTRANT_POSTTYPE_SLUG, $arrEntrantsSettings);

		// register the 'extravote' type
		$arrExtraVoteLabels			=	array(
											'name'					=> __('Extra Vote Questions'), 
											'singular_name'			=> __('Extra Vote Question'), 
											'add_new'				=> __('Add New'), 
											'add_new_item'			=> __('Add New Question'), 
											'edit_item'				=> __('Edit Question'), 
											'new_item'				=> __('New Question'), 
											'view_item'				=> __('View Question'), 
											'search_items'			=> __('Search Questions'), 
											'not_found'				=> __('Nothing found'), 
											'not_found_in_trash'	=> __('Nothing found in Trash'), 
											'parent_item_colon'		=> ''
									);
		$arrExtraVoteSettings		=	array(
											'labels'				=> $arrExtraVoteLabels, 
											'public'				=> true, 
											'publicly_queryable'	=> true, 
											'show_ui'				=> true, 
											'query_var'				=> true, 
											'menu_icon'				=> plugins_url('images/icon.png', __FILE__), 
											'rewrite'				=> array('slug' => self::EXTRAVOTE_POSTTYPE_SLUG), 
											'capability_type'		=> 'page', 
											'hierarchial'			=> false, 
											'menu_position'			=> NULL, 
											'supports'				=> array('title', 'editor', 'thumbnail')
										);
		register_post_type(self::EXTRAVOTE_POSTTYPE_SLUG, $arrExtraVoteSettings);
	}
	
	/**
	 * Runs when admin pages are initialized.
	 */
	public function admin_initialize() {
		if (((isset($_POST['nmeda_csv_export_dealercodes_nonce'])) && (wp_verify_nonce($_POST['nmeda_csv_export_dealercodes_nonce'], plugin_basename(__FILE__)))) || 
			((isset($_POST['nmeda_csv_export_entrantdata_nonce'])) && (wp_verify_nonce($_POST['nmeda_csv_export_entrantdata_nonce'], plugin_basename(__FILE__)))) || 
			((isset($_POST['nmeda_csv_export_votingallraw_nonce'])) && (wp_verify_nonce($_POST['nmeda_csv_export_votingallraw_nonce'], plugin_basename(__FILE__))))) {
			$this->_export_data_to_csv();
		}
			
		add_meta_box('entrant-details', 'Contest Entrant Information', array($this, 'metabox_entrant_details'), self::ENTRANT_POSTTYPE_SLUG, 'normal', 'core');
		add_meta_box('entrant-voting', 'Contest Entrant Voting Data', array($this, 'metabox_entrant_voting'), self::ENTRANT_POSTTYPE_SLUG, 'normal', 'core');

		$this->datasets['column_meta']				= $this->_get_entrant_column_metadata();
		
		add_filter('manage_entrant_posts_columns', array(&$this, 'column_entrant_header'), 1);
		add_filter('manage_entrant_posts_custom_column', array(&$this, 'column_entrant_column'), 1, 2);
		add_filter('pre_get_posts', array(&$this, 'manage_entrant_pre_get_posts'));
	}

	/**
	 * Runs when admin menus are initialized.
	 */
	public function admin_menu() {
		add_submenu_page('edit.php?post_type=' . self::ENTRANT_POSTTYPE_SLUG, 'Data Export Facilities', 'Data Export', 'add_users', self::ENTRANT_POSTTYPE_SLUG . '-data-export', array($this, 'admin_data_export'));
		add_submenu_page('edit.php?post_type=' . self::ENTRANT_POSTTYPE_SLUG, 'Dealer Information', 'Dealer Information', 'add_users', self::ENTRANT_POSTTYPE_SLUG . '-dealer-info', array($this, 'admin_dealer_info'));

		/* Add custom metadata to posts for "Recommended Post Date" */
		if(function_exists('x_add_metadata_group') && function_exists('x_add_metadata_field')) {
			x_add_metadata_group('nmeda_extravote_answerdata', self::EXTRAVOTE_POSTTYPE_SLUG, array(
				'label' => 'Answer Data'
			));

			// adds a text field to the first group
			x_add_metadata_field(self::EXTRAVOTE_FIELD_ANSWER1, self::EXTRAVOTE_POSTTYPE_SLUG, array(
				'group'			=> 'nmeda_extravote_answerdata', 
				'field_type'	=> 'text', 
				'label'			=> 'Answer #1'
			));
			x_add_metadata_field(self::EXTRAVOTE_FIELD_ANSWER2, self::EXTRAVOTE_POSTTYPE_SLUG, array(
				'group'			=> 'nmeda_extravote_answerdata', 
				'field_type'	=> 'text', 
				'label'			=> 'Answer #2'
			));
			x_add_metadata_field(self::EXTRAVOTE_FIELD_ANSWER3, self::EXTRAVOTE_POSTTYPE_SLUG, array(
				'group'			=> 'nmeda_extravote_answerdata', 
				'field_type'	=> 'text', 
				'label'			=> 'Answer #3'
			));
			x_add_metadata_field(self::EXTRAVOTE_FIELD_ANSWER4, self::EXTRAVOTE_POSTTYPE_SLUG, array(
				'group'			=> 'nmeda_extravote_answerdata', 
				'field_type'	=> 'text', 
				'label'			=> 'Answer #4'
			));
			x_add_metadata_field(self::EXTRAVOTE_FIELD_CORRECT, self::EXTRAVOTE_POSTTYPE_SLUG, array(
				'group'			=> 'nmeda_extravote_answerdata', 
				'field_type'	=> 'select', 
				'label'			=> 'Correct Answer #', 
				'values'		=> array('1' => 'Answer #1', '2' => 'Answer #2', '3' => 'Answer #3', '4' => 'Answer #4')
			));
			x_add_metadata_field(self::EXTRAVOTE_FIELD_DETAILS, self::EXTRAVOTE_POSTTYPE_SLUG, array(
				'group'			=> 'nmeda_extravote_answerdata', 
				'field_type'	=> 'wysiwyg', 
				'label'			=> 'Explanation of Answer'
			));
		}
	}
	
	/**
	 * Runs when admin pages are initialized.
	 */
	public function admin_header() {
	    global $current_screen;

	    switch ($current_screen->post_type) {
			case self::ENTRANT_POSTTYPE_SLUG : 
				add_editor_style('css/editor-style-entrants.css');
				break;
		}
    }

	/*	
	==========================================================================
	ADMIN COLUMN METHODS
	==========================================================================	
	*/
	public function column_entrant_header($arrColumns) {
		$arrNewColumns						= array();
		
		foreach ($arrColumns as $strKey => $strValue) {
			if ($strKey == 'date') {
				$arrNewColumns['entrant_citystate']		= 'City/State';
				$arrNewColumns['entrant_email']			= 'E-mail';
				$arrNewColumns['entrant_approved']		= 'Approved?';
				$arrNewColumns['entrant_status']		= 'Status';
			}
			$arrNewColumns[$strKey]					= $strValue;
		}
		
		return $arrNewColumns;
	}
	
	public function column_entrant_column($strColumnName, $intPostID) {
		$strColumnValue			= '';
		
		switch ($strColumnName) {
			case 'entrant_citystate' : 
				if (count($this->datasets['column_meta'])) {
					foreach ($this->datasets['column_meta'] as $objRow) {
						$intCurEntrantID		= intval($objRow->intEntrantID);
						
						if ($intCurEntrantID == $intPostID) {
							$strColumnValue		= stripslashes($objRow->strHeroCity) . ', ' . stripslashes($objRow->strHeroState);
							break;
						}
					}
				}
				break;

			case 'entrant_email' : 
				if (count($this->datasets['column_meta'])) {
					foreach ($this->datasets['column_meta'] as $objRow) {
						$intCurEntrantID		= intval($objRow->intEntrantID);
						
						if ($intCurEntrantID == $intPostID) {
							$strColumnValue		= stripslashes($objRow->strHeroEmail);
							break;
						}
					}
				}
				break;

			case 'entrant_approved' : 
				if (count($this->datasets['column_meta'])) {
					foreach ($this->datasets['column_meta'] as $objRow) {
						$intCurEntrantID		= intval($objRow->intEntrantID);
						
						if ($intCurEntrantID == $intPostID) {
							$strColumnValue		= ((intval($objRow->boolApproved)) ? 'Approved by Hero' : 'Pending Hero Approval');
							break;
						}
					}
				}
				break;

			case 'entrant_status' : 
				if (count($this->datasets['column_meta'])) {
					foreach ($this->datasets['column_meta'] as $objRow) {
						$intCurEntrantID		= intval($objRow->intEntrantID);
						
						if ($intCurEntrantID == $intPostID) {
							switch (strtolower($objRow->post_status)) {
								case 'private' : 
									$strColumnValue		= 'Private';
									break;
									
								case 'publish' : 
									$strColumnValue		= 'Public/Up for voting';
									break;
							}
							break;
						}
					}
				}
				break;
		}
		
		echo $strColumnValue;
	}
	
	public function manage_entrant_pre_get_posts($objQuery) {
		if ((is_admin()) && (isset($objQuery->query_vars['post_type']))) {
			switch ($objQuery->query_vars['post_type']) {
				case self::ENTRANT_POSTTYPE_SLUG : 
					$objQuery->set('orderby', 'title');
					$objQuery->set('order', 'ASC');
					break;
			}
		}
	}

	/*	
	==========================================================================
	ADMIN SAVE METHODS
	==========================================================================	
	*/
	
	/**
	 * Runs when posts are saved.
	 */
	public function admin_save($intPostID) {
		global $post;
		
		// save entrant metadata
		if ((isset($_POST['nmeda_entrant_meta_nonce'])) && (wp_verify_nonce($_POST['nmeda_entrant_meta_nonce'], plugin_basename(__FILE__)))) {
			if ((isset($_POST['nmeda_entrant_approved'])) && 
				(isset($_POST['nmeda_entrant_firstname'])) && 
				(isset($_POST['nmeda_entrant_lastname'])) && 
				(isset($_POST['nmeda_entrant_email'])) && 
				(isset($_POST['nmeda_entrant_address'])) && 
				(isset($_POST['nmeda_entrant_city'])) && 
				(isset($_POST['nmeda_entrant_state'])) && 
				(isset($_POST['nmeda_entrant_zip'])) && 
				(isset($_POST['nmeda_entrant_phone'])) && 
				(isset($_POST['nmeda_entrant_promocode'])) && 
				(isset($_POST['nmeda_entrant_video_url'])) && 
				(isset($_POST['nmeda_entrant_video_thumb_small'])) && 
				(isset($_POST['nmeda_entrant_video_thumb_large'])) && 
				(isset($_POST['nmeda_entrant_entered_ip'])) && 
				(isset($_POST['nmeda_entrant_entered_date']))) {
				$strQuery					= sprintf(	"DELETE FROM %snmam_entrant_meta WHERE intEntrantID = %d", 
														$this->db->prefix, 
														intval($intPostID));
				$this->db->query($strQuery);

				$strCurEntryURL				= get_permalink($intPostID);
				$boolCurIsApproved			= intval($_POST['nmeda_entrant_approved']);
				$strCurTitle				= $post->post_title;
				$strCurFirstName			= ucfirst(stripslashes($_POST['nmeda_entrant_firstname']));
				$strCurLastName				= ucfirst(stripslashes($_POST['nmeda_entrant_lastname']));
				$strCurEmail				= stripslashes($_POST['nmeda_entrant_email']);
				$strCurAddress				= stripslashes($_POST['nmeda_entrant_address']);
				$strCurCity					= ucfirst(stripslashes($_POST['nmeda_entrant_city']));
				$strCurState				= ucfirst(stripslashes($_POST['nmeda_entrant_state']));
				$strCurZIP					= stripslashes($_POST['nmeda_entrant_zip']);
				$strCurPhone				= stripslashes($_POST['nmeda_entrant_phone']);
				$strCurHeroFirstName		= ucfirst(stripslashes($_POST['nmeda_entrant_hero_firstname']));
				$strCurHeroLastName			= ucfirst(stripslashes($_POST['nmeda_entrant_hero_lastname']));
				$strCurHeroEmail			= stripslashes($_POST['nmeda_entrant_hero_email']);
				$strCurHeroAddress			= stripslashes($_POST['nmeda_entrant_hero_address']);
				$strCurHeroCity				= ucfirst(stripslashes($_POST['nmeda_entrant_hero_city']));
				$strCurHeroState			= ucfirst(stripslashes($_POST['nmeda_entrant_hero_state']));
				$strCurHeroZIP				= stripslashes($_POST['nmeda_entrant_hero_zip']);
				$strCurHeroPhone			= stripslashes($_POST['nmeda_entrant_hero_phone']);
				$strCurPromoCode			= stripslashes($_POST['nmeda_entrant_promocode']);
				$intCurMediaType			= intval($_POST['nmeda_entrant_mediatype']);
				$strCurVideoURL				= stripslashes($_POST['nmeda_entrant_video_url']);
				$strCurVideoThumbSmall		= stripslashes($_POST['nmeda_entrant_video_thumb_small']);
				$strCurVideoThumbLarge		= stripslashes($_POST['nmeda_entrant_video_thumb_large']);
				$boolCurHeroSemifinalist	= ((isset($_POST['nmeda_entrant_hero_semifinalist'])) ? 1 : 0);
				$strCurEnteredIP			= stripslashes($_POST['nmeda_entrant_entered_ip']);
				$dtCurEntered				= strtotime(stripslashes($_POST['nmeda_entrant_entered_date']));
			
				$strQuery					= sprintf(	"	INSERT INTO		%snmam_entrant_meta 
																			(	intEntrantID, 
																				boolApproved, 
																				strFirstName, 
																				strLastName, 
																				strEmail, 
																				strAddress, 
																				strCity, 
																				strState, 
																				strZIP, 
																				strPhone, 
																				strHeroFirstName, 
																				strHeroLastName, 
																				strHeroEmail, 
																				strHeroAddress, 
																				strHeroCity, 
																				strHeroState, 
																				strHeroZIP, 
																				strHeroPhone, 
																				boolHeroSemifinalist, 
																				strVideoURL, 
																				strVideoThumbSmall, 
																				strVideoThumbLarge, 
																				strPromoCode, 
																				strEnteredIP, 
																				dtEntered)
																			VALUES
																			(
																				%d, 
																				%d, 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				%d, 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				'%s', 
																				NOW()
																			)", 
																			$this->db->prefix, 
																			intval($intPostID), 
																			intval($boolCurIsApproved), 
																			$this->db->escape($strCurFirstName), 
																			$this->db->escape($strCurLastName), 
																			$this->db->escape($strCurEmail), 
																			$this->db->escape($strCurAddress), 
																			$this->db->escape($strCurCity), 
																			$this->db->escape($strCurState), 
																			$this->db->escape($strCurZIP), 
																			$this->db->escape($strCurPhone), 
																			$this->db->escape($strCurHeroFirstName), 
																			$this->db->escape($strCurHeroLastName), 
																			$this->db->escape($strCurHeroEmail), 
																			$this->db->escape($strCurHeroAddress), 
																			$this->db->escape($strCurHeroCity), 
																			$this->db->escape($strCurHeroState), 
																			$this->db->escape($strCurHeroZIP), 
																			$this->db->escape($strCurHeroPhone), 
																			intval($boolCurHeroSemifinalist), 
																			$this->db->escape($strCurVideoURL), 
																			$this->db->escape($strCurVideoThumbSmall), 
																			$this->db->escape($strCurVideoThumbLarge), 
																			$this->db->escape($strCurPromoCode), 
																			$this->db->escape($strCurEnteredIP), 
																			$this->db->escape($dtCurEntered));
				$this->db->query($strQuery);
				
				// update meta
				update_post_meta($intPostID, self::ENTRANT_META_HERO_FIRST, $strCurHeroFirstName);
				update_post_meta($intPostID, self::ENTRANT_META_HERO_LAST, $strCurHeroLastName);
				update_post_meta($intPostID, self::ENTRANT_META_HERO_CITY, $strCurHeroCity);
				update_post_meta($intPostID, self::ENTRANT_META_HERO_STATE, $strCurHeroState);
				
				// overwrite featured image with video thumbnail image?
				if ((isset($_POST['nmeda_entrant_video_thumbnail_overwrite'])) && 
					($intCurMediaType == self::ENTRANT_MEDIA_TYPE_VIDEO) && 
					(strlen($strCurVideoURL)) && 
					(strlen($strCurVideoThumbLarge))) {
					$this->_set_entry_photo($intPostID, $strCurTitle, self::ENTRANT_MEDIA_TYPE_VIDEO, '', $strCurVideoThumbLarge);
				}
				
				if (($post->post_type = self::ENTRANT_POSTTYPE_SLUG) && ($post->post_status == 'publish') && ($boolCurIsApproved) && (strlen($strCurEntryURL)) && (strlen($strCurHeroEmail)) && (strlen(get_post_meta($intPostID, self::ENTRANT_META_NOTIFY_PUB, true)) == 0)) {
					// send 'entry live' e-mail
					$strQuery			= sprintf(	"SELECT post_title, post_content FROM %sposts WHERE ID = (SELECT post_id FROM %spostmeta WHERE meta_key = 'email_template_type' AND meta_value = 'published') LIMIT 1", 
													$this->db->prefix, 
													$this->db->prefix);
					$queryGetEmail		= $this->db->get_results($strQuery);
					
					if (count($queryGetEmail)) {
						$objRow				= $queryGetEmail[0];
						
						$strMailSubject		= stripslashes($objRow->post_title);
						$txtMailMessage		= wpautop(stripslashes($objRow->post_content));
						$txtMailMessage		= str_replace('[LINK]', '<a href="' . $strCurEntryURL . '">' . $strCurEntryURL . '</a>', $txtMailMessage);
						
						$arrMailHeaders		= array('From: NMEDA.com Mobility Awareness Month <info@mobilityawarenessmonth.com>');
						
						if (filter_var($strCurHeroEmail, FILTER_VALIDATE_EMAIL)) {
							wp_mail(	$strCurHeroEmail, 
										$strMailSubject, 
										$txtMailMessage, 
										$arrMailHeaders);
						}

						// mark e-mail as sent
						update_post_meta($intPostID, self::ENTRANT_META_NOTIFY_PUB, '1');
					}
					unset($queryGetEmail);
				}
			}
		}
	}
	
	/*	
	==========================================================================
	ADMIN META METHODS
	==========================================================================	
	*/
	public function metabox_entrant_details($objPost) {
		wp_nonce_field(plugin_basename(__FILE__), 'nmeda_entrant_meta_nonce');

		$boolCurIsApproved			= 1;
		$strCurFirstName			= '';
		$strCurLastName				= '';
		$strCurEmail				= '';
		$strCurAddress				= '';
		$strCurCity					= '';
		$strCurState				= '';
		$strCurZIP					= '';
		$strCurPhone				= '';
		$strCurHeroFirstName		= '';
		$strCurHeroLastName			= '';
		$strCurHeroEmail			= '';
		$strCurHeroAddress			= '';
		$strCurHeroCity				= '';
		$strCurHeroState			= '';
		$strCurHeroZIP				= '';
		$strCurHeroPhone			= '';
		$boolCurHeroSemifinalist	= false;
		$strCurPromoCode			= '';
		$strCurVideoURL				= '';
		$strCurVideoID				= '';
		$strCurVideoThumbSmall		= '';
		$strCurVideoThumbLarge		= '';
		$strCurEnteredIP			= '';
		$dtCurEntered				= time();

		$strQuery					= sprintf(	"SELECT * FROM %snmam_entrant_meta WHERE intEntrantID = %d LIMIT 1", 
												$this->db->prefix, 
												intval($objPost->ID));
		$queryGetEntrantMeta		= $this->db->get_results($strQuery);
		
		if (count($queryGetEntrantMeta)) {
			$objRow						= $queryGetEntrantMeta[0];
			
			$boolCurIsApproved			= intval($objRow->boolApproved);
			$strCurFirstName			= stripslashes($objRow->strFirstName);
			$strCurLastName				= stripslashes($objRow->strLastName);
			$strCurEmail				= stripslashes($objRow->strEmail);
			$strCurAddress				= stripslashes($objRow->strAddress);
			$strCurCity					= stripslashes($objRow->strCity);
			$strCurState				= stripslashes($objRow->strState);
			$strCurZIP					= stripslashes($objRow->strZIP);
			$strCurPhone				= stripslashes($objRow->strPhone);
			$strCurHeroFirstName		= stripslashes($objRow->strHeroFirstName);
			$strCurHeroLastName			= stripslashes($objRow->strHeroLastName);
			$strCurHeroEmail			= stripslashes($objRow->strHeroEmail);
			$strCurHeroAddress			= stripslashes($objRow->strHeroAddress);
			$strCurHeroCity				= stripslashes($objRow->strHeroCity);
			$strCurHeroState			= stripslashes($objRow->strHeroState);
			$strCurHeroZIP				= stripslashes($objRow->strHeroZIP);
			$strCurHeroPhone			= stripslashes($objRow->strHeroPhone);
			$boolCurHeroSemifinalist	= intval($objRow->boolHeroSemifinalist);
			$strCurPromoCode			= stripslashes($objRow->strPromoCode);
			$strCurVideoURL				= stripslashes($objRow->strVideoURL);
			$strCurVideoID				= ((strlen($strCurVideoURL)) ? $this->_get_youtube_video_id($strCurVideoURL) : '');
			$strCurVideoThumbSmall		= stripslashes($objRow->strVideoThumbSmall);
			$strCurVideoThumbLarge		= stripslashes($objRow->strVideoThumbLarge);
			$intCurMediaType			= ((strlen($strCurVideoURL)) ? self::ENTRANT_MEDIA_TYPE_VIDEO : self::ENTRANT_MEDIA_TYPE_PHOTO);
			$strCurEnteredIP			= stripslashes($objRow->strEnteredIP);
			$dtCurEntered				= strtotime(stripslashes($objRow->dtEntered));
		}
		unset($queryGetEntrantMeta);

		include plugin_dir_path(__FILE__) . 'views/admin-entrant-details.phtml';
	}

	public function metabox_entrant_voting($objPost) {
		wp_nonce_field(plugin_basename(__FILE__), 'nmeda_entrant_voting_nonce');
		
		$intNumVotes				= intval(get_post_meta($objPost->ID, self::ENTRANT_META_VOTES, true));
		$intNumVotesBonus			= $this->_get_bonus_votes($objPost->ID);
		
		include plugin_dir_path(__FILE__) . 'views/admin-entrant-voting.phtml';
	}

	public function admin_data_export() {
		$strNonceScriptName			= plugin_basename(__FILE__);
		
		include plugin_dir_path(__FILE__) . 'views/admin-data-export.phtml';
	}

	public function admin_dealer_info() {
		ini_set("display_errors", "on");
		error_reporting(E_ALL);
		
		require_once plugin_dir_path(__FILE__) . 'include/admin-dealerinfo.php';

		$strNonceScriptName			= plugin_basename(__FILE__);

		$objAdmin					= new NMEDA_MAM_DealerInfo();
		$strActionType				= ((isset($_REQUEST['action'])) ? strtolower($_REQUEST['action']) : '');
		
		switch ($strActionType) {
			case 'commit' : 
				$objAdmin->commit();
				break;
				
			case 'insert' :
				$objAdmin->insert();
				break;
				
			case 'remove' : 
				$objAdmin->remove();
				break;
				
			case 'edit' : 
				$objAdmin->edit();
				break;
				
			case 'add' : 
				$objAdmin->add();
				break;
				
			default : 
				$objAdmin->index();
				break;
		}
	}

	/*	
	==========================================================================
	ADMIN EXPORT TO CSV METHODS
	==========================================================================	
	*/
	private function _export_data_to_csv() {
/*
	set_time_limit(0);
	ignore_user_abort(1);
*/	
	
		if ((isset($_POST['action'])) && (strlen($_POST['action']))) {
			$strExportType			= strtolower($_POST['action']);
			
			switch ($strExportType) {
				case 'entrant-data' : 
					$txtCSVData				= 'First Name,Last Name,E-mail,Phone,Address,City,State,ZIP,Dealer Code,IP Address,Date Entered' . PHP_EOL;
					
					$strQuery				= sprintf(	"SELECT strHeroFirstName, strHeroLastName, strHeroEmail, strHeroPhone, strHeroAddress, strHeroCity, strHeroState, strHeroZIP, strPromoCode, strEnteredIP, dtEntered FROM %snmam_entrant_meta WHERE intEntrantID IN (SELECT ID FROM %sposts WHERE post_type = '%s' AND ((post_status = 'private') OR (post_status = 'publish'))) ORDER BY dtEntered DESC", 
														$this->db->prefix, 
														$this->db->prefix, 
														self::ENTRANT_POSTTYPE_SLUG);
					$queryGetEntrantInfo	= $this->db->get_results($strQuery);
					
					if (count($queryGetEntrantInfo)) {
						foreach ($queryGetEntrantInfo as $objRow) {
							$strCurEntrantFirstName		= stripslashes($objRow->strHeroFirstName);
							$strCurEntrantLastName		= stripslashes($objRow->strHeroLastName);
							$strCurEntrantEmail			= stripslashes($objRow->strHeroEmail);
							$strCurEntrantPhone			= stripslashes($objRow->strHeroPhone);
							$strCurEntrantAddress		= stripslashes($objRow->strHeroAddress);
							$strCurEntrantCity			= stripslashes($objRow->strHeroCity);
							$strCurEntrantState			= stripslashes($objRow->strHeroState);
							$strCurEntrantZIP			= stripslashes($objRow->strHeroZIP);
							$strCurEntrantPromoCode		= strtoupper(stripslashes($objRow->strPromoCode));
							$strCurEntrantIP			= stripslashes($objRow->strEnteredIP);
							$dtCurEnteredDate			= date('F, jS Y g:i a e', strtotime(stripslashes($objRow->dtEntered)));
							
							$txtCSVData					.= sprintf(	'"%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s"', 
																	$strCurEntrantFirstName, 
																	$strCurEntrantLastName, 
																	$strCurEntrantEmail, 
																	$strCurEntrantPhone, 
																	$strCurEntrantAddress, 
																	$strCurEntrantCity, 
																	$strCurEntrantState, 
																	$strCurEntrantZIP,
																	$strCurEntrantPromoCode,  
																	$strCurEntrantIP, 
																	$dtCurEnteredDate) . PHP_EOL;
						}
					}
					
					// output it
					header('Content-Type: text/csv');
					header('Content-Disposition: filename=entrant-data-' . date('Y-m-d') . '.csv');
					echo $txtCSVData;
					die();
					break;
					
				case 'dealer-code' : 
					// get dealer information for all codes in use
					$txtTableHTML		= '';
					
					$strQuery			= sprintf(	"SELECT DISTINCT strName, strCode, strAddress1, strAddress2, strCity, strState, strZIP, strEmail, strPhone FROM %snmam_dealers WHERE strCode IN (SELECT strPromoCode FROM %snmam_entrant_meta WHERE LENGTH(strPromoCode) > 0 AND intEntrantID IN (SELECT ID FROM %sposts WHERE post_type = '%s' AND ((post_status = 'private') OR (post_status = 'publish')))) ORDER BY strCode ASC", 
													$this->db->prefix, 
													$this->db->prefix, 
													$this->db->prefix, 
													self::ENTRANT_POSTTYPE_SLUG);
					$queryGetDealerInfo	= $this->db->get_results($strQuery);
					
					if (count($queryGetDealerInfo)) {
						$strQuery				= sprintf(	"SELECT strHeroFirstName, strHeroLastName, strHeroEmail, strHeroPhone, strHeroAddress, strHeroCity, strHeroState, strHeroZIP, strPromoCode, strEnteredIP, dtEntered FROM %snmam_entrant_meta WHERE intEntrantID IN (SELECT ID FROM %sposts WHERE post_type = '%s' AND ((post_status = 'private') OR (post_status = 'publish'))) AND LENGTH(strPromoCode) > 0", 
															$this->db->prefix, 
															$this->db->prefix, 
															self::ENTRANT_POSTTYPE_SLUG);
						$queryGetEntrantInfo	= $this->db->get_results($strQuery);
					
						if (count($queryGetEntrantInfo)) {
							foreach ($queryGetDealerInfo as $objRow) {
								$strCurDealerName		= stripslashes($objRow->strName);
								$strCurDealerCode		= stripslashes($objRow->strCode);
								$strCurDealerAddress1	= stripslashes($objRow->strAddress1);
								$strCurDealerAddress2	= stripslashes($objRow->strAddress2);
								$strCurDealerCity		= stripslashes($objRow->strCity);
								$strCurDealerState		= stripslashes($objRow->strState);
								$strCurDealerZIP		= stripslashes($objRow->strZIP);
								$strCurDealerEmail		= stripslashes($objRow->strEmail);
								$strCurDealerPhone		= stripslashes($objRow->strPhone);
								
								$txtTableHTML			.=	'<tr>' . 
																'<td colspan="11" style="font-size: 16px; font-weight: bold;">' . $strCurDealerCode . ' -- ' . $strCurDealerName . '</td>' . 
															'</tr><tr>' . 
																'<td colspan="11">' . 
																	$strCurDealerAddress1 . ((strlen($strCurDealerAddress2)) ? (', ' . $strCurDealerAddress2) : '') . 
																	' &bull; ' . 
																	$strCurDealerCity . ', ' . $strCurDealerState . '  ' . $strCurDealerZIP . 
																	((strlen($strCurDealerPhone)) . (' &bull; Phone: ' . $strCurDealerPhone) . '') . 
																	((strlen($strCurDealerEmail)) . (' &bull; E-mail: ' . $strCurDealerEmail) . '') . 
																'</td>' . 
															'</tr><tr>' . 
																'<td colspan="11"></td>' . 
															'</tr><tr>' . 
																'<td width="100"></td>' . 
																'<td align="left"><strong>First Name</strong></td>' . 
																'<td align="left"><strong>Last Name</strong></td>' . 
																'<td align="left"><strong>E-mail</strong></td>' . 
																'<td align="left"><strong>Phone</strong></td>' . 
																'<td align="left"><strong>Address</strong></td>' . 
																'<td align="left"><strong>City</strong></td>' . 
																'<td align="left"><strong>State</strong></td>' . 
																'<td align="left"><strong>ZIP</strong></td>' . 
																'<td align="left"><strong>IP Address</strong></td>' . 
																'<td align="left"><strong>Date Entered</strong></td>' . 
															'</tr>';
								
								foreach ($queryGetEntrantInfo as $objRow) {
									$strCurEntrantPromoCode		= strtoupper(stripslashes($objRow->strPromoCode));
									
									if ($strCurEntrantPromoCode == $strCurDealerCode) {
										$strCurEntrantFirstName		= stripslashes($objRow->strHeroFirstName);
										$strCurEntrantLastName		= stripslashes($objRow->strHeroLastName);
										$strCurEntrantEmail			= stripslashes($objRow->strHeroEmail);
										$strCurEntrantPhone			= stripslashes($objRow->strHeroPhone);
										$strCurEntrantAddress		= stripslashes($objRow->strHeroAddress);
										$strCurEntrantCity			= stripslashes($objRow->strHeroCity);
										$strCurEntrantState			= stripslashes($objRow->strHeroState);
										$strCurEntrantZIP			= stripslashes($objRow->strHeroZIP);
										$strCurEntrantIP			= stripslashes($objRow->strEnteredIP);
										$dtCurEnteredDate			= date('F, jS Y g:i a', strtotime(stripslashes($objRow->dtEntered)));
										
										$txtTableHTML				.=	'<tr>' .
																			'<td width="100"></td>' . 
																			'<td align="left">' . $strCurEntrantFirstName . '</td>' . 
																			'<td align="left">' . $strCurEntrantLastName . '</td>' . 
																			'<td align="left">' . $strCurEntrantEmail . '</td>' . 
																			'<td align="left">' . $strCurEntrantPhone . '</td>' . 
																			'<td align="left">' . $strCurEntrantAddress . '</td>' . 
																			'<td align="left">' . $strCurEntrantCity . '</td>' . 
																			'<td align="left">' . $strCurEntrantState . '</td>' . 
																			'<td align="left">' . $strCurEntrantZIP . '</td>' . 
																			'<td align="left">' . $strCurEntrantIP . '</td>' . 
																			'<td align="left">' . $dtCurEnteredDate . '</td>' . 
																		'</tr>';
									}
								}
								
								$txtTableHTML			.=	'<tr>' . 
																'<td colspan="11"></td>' . 
															'</tr><tr>' . 
																'<td colspan="11"></td>' . 
															'</tr>';
							}
						}
						
						$txtTableHTML				=	'<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">' . 
															'<head>' . 
																'<meta http-equiv=Content-Type content="text/html; charset=windows-1252">' . 
																'<meta name=ProgId content=Excel.Sheet>' . 
																'<meta name=Generator content="Microsoft Excel 11">' . 
																'<style>' . 
																	'<!--table' . PHP_EOL . 
																	'@page{}' . PHP_EOL . 
																	'-->' . PHP_EOL . 
																'</style>' . 
																'<!--[if gte mso 9]><xml>' . PHP_EOL . 
																	'<x:ExcelWorkbook>' . PHP_EOL . 
																		'<x:ExcelWorksheets>' . PHP_EOL . 
																			'<x:ExcelWorksheet>' . PHP_EOL . 
																				'<x:Name>Dealer Codes In Use</x:Name>' . PHP_EOL . 
																				'<x:WorksheetOptions>' . PHP_EOL . 
																					'<x:Panes></x:Panes>' . PHP_EOL . 
																				'</x:WorksheetOptions>' . PHP_EOL . 
																			'</x:ExcelWorksheet>' . PHP_EOL . 
																		'</x:ExcelWorksheets>' . PHP_EOL . 
																	'</x:ExcelWorkbook>' . PHP_EOL . 
																'</xml><![endif]-->' . PHP_EOL . 
															'</head>' . 
															'<body>' .
																'<h1 align="center">Dealer Codes in Use - ' . date('F jS, Y g:i a e') . '</h1>' . 
																'<table>' . 
																$txtTableHTML . 
																'</table>' . 
															'</body>' . 
														'</html>';
						
						// output it
						header('Content-Type: application/vnd.ms-excel');
						header('Content-Disposition: filename=dealer-codes-inuse-' . date('Y-m-d') . '.xls');
						print $txtTableHTML;
					}
					break;

				case 'voting-data-raw' : 
					// get all voting data
					$txtTableHTML		= '';
					$arrVotingData		= array();
					$strFilterType		= ((isset($_POST['strFilterOption'])) ? strtolower($_POST['strFilterOption']) : '');
					$strHeaderText		= '';
					$strFilename		= '';
					
					switch ($strFilterType) {
						case 'top' : 
							$strHeaderText		= 'Voting Data Export of the Top 5% (20%) Entrants as of ' . date('F jS, Y g:i a e');
							$strFilename		= 'voting-top20pct-export-' . strtolower(date('Y-m-d-g-iae')) . '.xls';
							$strQuery			= sprintf(	"SELECT DISTINCT v.intEntrantID, e.strHeroFirstName, e.strHeroLastName FROM %snmam_entrant_votes v LEFT JOIN %snmam_entrant_meta e ON v.intEntrantID = e.intEntrantID",
							/* $strQuery			= sprintf(	"SELECT DISTINCT v.intEntrantID, e.strHeroFirstName, e.strHeroLastName FROM %snmam_entrant_meta v LEFT JOIN %snmam_entrant_votes e ON v.intEntrantID = e.intEntrantID", */ 							
															$this->db->prefix, 
															$this->db->prefix);
							break;
						
						default : 
							$strHeaderText		= 'Complete Voting Data Export as of ' . date('F jS, Y g:i a e');
							$strFilename		= 'voting-data-full-export-' . strtolower(date('Y-m-d-g-iae')) . '.xls';
							$strQuery			= sprintf(	"SELECT DISTINCT v.intEntrantID, e.strHeroFirstName, e.strHeroLastName FROM %snmam_entrant_votes v LEFT JOIN %snmam_entrant_meta e ON v.intEntrantID = e.intEntrantID",
							/* $strQuery			= sprintf(	"SELECT DISTINCT v.intEntrantID, e.strFirstName, e.strLastName FROM %snmam_entrant_votes v LEFT JOIN %snmam_entrant_meta e ON v.intEntrantID = e.intEntrantID", */
															$this->db->prefix, 
															$this->db->prefix);
							break;
					}
					$queryGetEntrants	= $this->db->get_results($strQuery);
					
					/* commented out to line 1025 */
					if (count($queryGetEntrants)) {
						$strQuery				= sprintf(	"SELECT intEntrantID, dtDate, strIPAddress, strFirstName, strLastName, strEmail, txtAdditionalInfo, boolSubscribe, intVotes FROM %snmam_entrant_votes ORDER BY dtDate DESC", 
															$this->db->prefix);
						$queryGetVotes			= $this->db->get_results($strQuery);
					
						if (count($queryGetVotes)) {
							foreach ($queryGetEntrants as $objRow) {
								$intCurEntrantID		= intval($objRow->intEntrantID);
								$strCurHeroFirstName	= stripslashes($objRow->strHeroFirstName);
								$strCurHeroLastName		= stripslashes($objRow->strHeroLastName);
								$intCurEntrantVotes		= 0;
								$strCurEntrantPermalink	= get_permalink($intCurEntrantID);
								$arrCurEntrantVoters	= array();
								
								foreach ($queryGetVotes as $objRow) {
									$intCurVoteEntrantID	= intval($objRow->intEntrantID);
									
									if ($intCurVoteEntrantID == $intCurEntrantID) {
										$dtCurVoteDate			= stripslashes($objRow->dtDate);
										$strCurVoteIPAddress	= stripslashes($objRow->strIPAddress);
										$strCurVoteFirstName	= stripslashes($objRow->strFirstName);
										$strCurVoteLastName		= stripslashes($objRow->strLastName);
										$strCurVoteEmail		= stripslashes($objRow->strEmail);
										$strCurVoteAdditional	= stripslashes($objRow->txtAdditionalInfo);
										$strCurVoteAdditional	= ((strlen($strCurVoteAdditional)) ? implode(', ', unserialize($strCurVoteAdditional)) : '');
										$boolCurVoteSubscribe	= intval($objRow->boolSubscribe);
										$intCurVoteCount		= intval($objRow->intVotes);
										
										array_push(	$arrCurEntrantVoters, 
													array(
														'date'			=> $dtCurVoteDate, 
														'ip'			=> $strCurVoteIPAddress, 
														'firstname'		=> $strCurVoteFirstName, 
														'lastname'		=> $strCurVoteLastName, 
														'email'			=> $strCurVoteEmail, 
														'additional'	=> $strCurVoteAdditional, 
														'subscribed'	=> (($boolCurVoteSubscribe) ? 'Yes' : 'No'), 
														'count'			=> $intCurVoteCount
													));
													
										$intCurEntrantVotes		+= $intCurVoteCount;
									}
								}
								
								$txtTableHTML			.=	'<tr>' . 
																'<td colspan="9" style="font-size: 16px; font-weight: bold;">ID#' . $intCurEntrantID . ': ' . $strCurHeroFirstName . ' ' . $strCurHeroLastName . '</td>' . 
															'</tr><tr>' . 
																'<td colspan="9" style="font-size: 14px; font-weight: bold;"><u>' . $intCurEntrantVotes . ' Votes</u> (<a href="' . $strCurEntrantPermalink . '">' . $strCurEntrantPermalink . '</a>)</td>' . 
															'</tr><tr>' . 
																'<td colspan="9"></td>' . 
															'</tr><tr>' . 
																'<td width="100"></td>' . 
																'<td align="left"><strong>Date Voted</strong></td>' . 
																'<td align="left"><strong>IP Address</strong></td>' . 
																'<td align="left"><strong>#/Votes</strong></td>' . 
																'<td align="left"><strong>First Name</strong></td>' . 
																'<td align="left"><strong>Last Name</strong></td>' . 
																'<td align="left"><strong>E-mail</strong></td>' . 
																'<td align="left"><strong>Subscribed?</strong></td>' . 
																'<td align="left"><strong>Additional Info.</strong></td>' . 
															'</tr>';
								
								foreach ($arrCurEntrantVoters as $arrVote) {
									$txtTableHTML				.=	'<tr>' .
																		'<td width="100"></td>' . 
																		'<td align="left">' . $arrVote['date'] . '</td>' . 
																		'<td align="left">' . $arrVote['ip'] . '</td>' . 
																		'<td align="left">' . $arrVote['count'] . '</td>' . 
																		'<td align="left">' . $arrVote['firstname'] . '</td>' . 
																		'<td align="left">' . $arrVote['lastname'] . '</td>' . 
																		'<td align="left">' . $arrVote['email'] . '</td>' . 
																		'<td align="left">' . $arrVote['subscribed'] . '</td>' . 
																		'<td align="left">' . $arrVote['additional'] . '</td>' . 
																	'</tr>';
								}
								
								$txtTableHTML			.=	'<tr>' . 
																'<td colspan="9"></td>' . 
															'</tr><tr>' . 
																'<td colspan="9"></td>' . 
															'</tr>';
							}
						}
					}
					/* commented out */
					
					$txtTableHTML				=	'<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">' . 
														'<head>' . 
															'<meta http-equiv=Content-Type content="text/html; charset=windows-1252">' . 
															'<meta name=ProgId content=Excel.Sheet>' . 
															'<meta name=Generator content="Microsoft Excel 11">' . 
															'<style>' . 
																'<!--table' . PHP_EOL . 
																'@page{}' . PHP_EOL . 
																'-->' . PHP_EOL . 
															'</style>' . 
															'<!--[if gte mso 9]><xml>' . PHP_EOL . 
																'<x:ExcelWorkbook>' . PHP_EOL . 
																	'<x:ExcelWorksheets>' . PHP_EOL . 
																		'<x:ExcelWorksheet>' . PHP_EOL . 
																			'<x:Name>Dealer Codes In Use</x:Name>' . PHP_EOL . 
																			'<x:WorksheetOptions>' . PHP_EOL . 
																				'<x:Panes></x:Panes>' . PHP_EOL . 
																			'</x:WorksheetOptions>' . PHP_EOL . 
																		'</x:ExcelWorksheet>' . PHP_EOL . 
																	'</x:ExcelWorksheets>' . PHP_EOL . 
																'</x:ExcelWorkbook>' . PHP_EOL . 
															'</xml><![endif]-->' . PHP_EOL . 
														'</head>' . 
														'<body>' .
															'<h1 align="center">' . $strHeaderText . '</h1>' . 
															'<h3 align="center">Entrants are sorted by number of total votes (in descending order)</h3>' . 
															'<table>' . 
															$txtTableHTML . 
															'</table>' . 
														'</body>' . 
													'</html>';
					
					// output it
					header('Content-Type: application/vnd.ms-excel');
					header('Content-Disposition: filename=' . $strFilename);
					print $txtTableHTML;
					break;
			}
		}
		die();
	}

	/*	
	==========================================================================
	FRONT-END AND DATA ACCESS METHODS
	==========================================================================	
	*/
	public function verify_nominated_approval_keys($strKey, $strKeyedEmail) {
		$intEntrantID			= 0;
		
		if ((strlen($strKey)) && (strlen($strKeyedEmail))) {
			// get salt and e-mail
			$strQuery					= sprintf(	"	SELECT k.intEntrantID, k.strSalt, m.strHeroEmail 
														FROM %snmam_entrant_preview_keys k 
														LEFT JOIN %snmam_entrant_meta m ON k.intEntrantID = m.intEntrantID 
														WHERE k.strKey = '%s' 
														AND k.strKeyedEmail = '%s' 
														LIMIT 1", 
													$this->db->prefix, 
													$this->db->prefix, 
													$this->db->escape($strKey), 
													$this->db->escape($strKeyedEmail));
			$queryGetEntrantData		= $this->db->get_results($strQuery);
			
			if (count($queryGetEntrantData)) {
				$objRow						= $queryGetEntrantData[0];
				
				$intCurEntrantID			= intval($objRow->intEntrantID);
				$strCurEntrantSalt			= stripslashes($objRow->strSalt);
				$strCurEntrantEmail			= stripslashes($objRow->strHeroEmail);
				
				$strCurEntrantKeyedEmail	= md5($strCurEntrantSalt . strtolower($strCurEntrantEmail) . $strKey);
				
				if ($strCurEntrantKeyedEmail == $strKeyedEmail) {
					$intEntrantID				= $intCurEntrantID;
				}
			}
			unset($queryGetEntrantData);
		}
		
		return $intEntrantID;
	}
	
	public function get_entrant_data_by_id($intEntrantID, $boolForceLookup = false, $boolNeedStoryText = true) {
		$arrReturnData				=	array(
											'status'				=> self::ENTRANT_STATUS_NOT_FOUND, 
											"firstname"				=> '', 
											'lastname'				=> '', 
											'email'					=> '', 
											'address'				=> '', 
											'city'					=> '', 
											'state'					=> '', 
											'zip'					=> '', 
											'phone'					=> '', 
											'semifinalist'			=> false, 
											'media_type'			=> '', 
											'photo_url'				=> '', 
											'photo_url_raw'			=> '', 
											'video_url'				=> '', 
											'video_thumb_small'		=> '', 
											'video_thumb_large'		=> '', 
											'votes'					=> 0
										);

		// Testing 3/28 @ 3:50PM: Showing rows 1 - 0 ( 1 total, Query took 0.0003 sec)
		$strQuery					= sprintf(	"SELECT * FROM %snmam_entrant_meta WHERE intEntrantID = %d LIMIT 1", 
												$this->db->prefix, 
												intval($intEntrantID));
		$queryGetEntrantMeta		= $this->db->get_results($strQuery);
		
		/*
		// Testing 3/28 @ 3:50PM: Showing rows 1 - 0 ( 1 total, Query took 0.0005 sec)
		$strQuery					= sprintf(	"SELECT m.*, p.post_content FROM %snmam_entrant_meta m LEFT JOIN %sposts p ON m.intEntrantID = p.ID WHERE m.intEntrantID = %d LIMIT 1", 
												$this->db->prefix, 
												$this->db->prefix, 
												intval($intEntrantID));
		$queryGetEntrantMeta		= $this->db->get_results($strQuery);
		*/
		
		if (count($queryGetEntrantMeta)) {
			$objRow						= $queryGetEntrantMeta[0];
			
			$boolCurIsApproved			= intval($objRow->boolApproved);
			
			if (($boolCurIsApproved === 0) || ($boolForceLookup === true)) {
				$strCurHeroFirstName		= stripslashes($objRow->strHeroFirstName);
				$strCurHeroLastName			= stripslashes($objRow->strHeroLastName);
				$strCurHeroEmail			= stripslashes($objRow->strHeroEmail);
				$strCurHeroAddress			= stripslashes($objRow->strHeroAddress);
				$strCurHeroCity				= stripslashes($objRow->strHeroCity);
				$strCurHeroState			= stripslashes($objRow->strHeroState);
				$strCurHeroZIP				= stripslashes($objRow->strHeroZIP);
				$strCurHeroPhone			= stripslashes($objRow->strHeroPhone);
				$boolCurHeroSemifinalist	= intval($objRow->boolHeroSemifinalist);
				$strCurPromoCode			= stripslashes($objRow->strPromoCode);
				$txtCurStory				= '';
				$strCurPhotoURL				= get_the_post_thumbnail($intEntrantID, 'entrants-profile');
				$strCurPhotoURLRaw			= '';
				$strCurVideoURL				= stripslashes($objRow->strVideoURL);
				$strCurVideoID				= ((strlen($strCurVideoURL)) ? $this->_get_youtube_video_id($strCurVideoURL) : '');
				$strCurVideoThumbSmall		= stripslashes($objRow->strVideoThumbSmall);
				$strCurVideoThumbLarge		= stripslashes($objRow->strVideoThumbLarge);
				$intCurMediaType			= ((strlen($strCurVideoURL)) ? self::ENTRANT_MEDIA_TYPE_VIDEO : self::ENTRANT_MEDIA_TYPE_PHOTO);
				$intCurVoteCount			= intval(get_post_meta($intEntrantID, self::ENTRANT_META_VOTES, true));
				$intCurVoteBonusCount		= $this->_get_bonus_votes($intEntrantID);
				$strCurEnteredIP			= stripslashes($objRow->strEnteredIP);
				$dtCurEntered				= strtotime(stripslashes($objRow->dtEntered));
				
				if (strlen($strCurPhotoURL)) {
					$strPattern					= '/src="([^"]*)"/';

					preg_match($strPattern, $strCurPhotoURL, $arrMatches);
					
					if (count($arrMatches) >= 2) {
						$strCurPhotoURLRaw			= $arrMatches[1];
					}

					unset($arrMatches);
				}
				
				// get and format story text
				// Testing 3/28 @ 3:50PM: Showing rows 1 - 0 ( 1 total, Query took 0.0002 sec)
				if ($boolNeedStoryText) {
					$strQuery					= sprintf(	"SELECT * FROM %sposts WHERE ID = %d LIMIT 1", 
															$this->db->prefix, 
															intval($intEntrantID));
					$queryGetEntrantStory		= $this->db->get_results($strQuery);
					
					if (count($queryGetEntrantStory)) {
						$txtCurStory				= stripslashes($queryGetEntrantStory[0]->post_content);
					}
					unset($queryGetEntrantStory);
				}
				
				$arrReturnData				=	array(
													'status'				=> self::ENTRANT_STATUS_FOUND, 
													"firstname"				=> $strCurHeroFirstName, 
													'lastname'				=> $strCurHeroLastName, 
													'email'					=> $strCurHeroEmail, 
													'address'				=> $strCurHeroAddress, 
													'city'					=> $strCurHeroCity, 
													'state'					=> $strCurHeroState, 
													'zip'					=> $strCurHeroZIP, 
													'phone'					=> $strCurHeroPhone, 
													'semifinalist'			=> $boolCurHeroSemifinalist, 
													'story'					=> $txtCurStory, 
													'story_html'			=> wpautop($txtCurStory), 
													'media_type'			=> $intCurMediaType, 
													'photo_url'				=> $strCurPhotoURL, 
													'photo_url_raw'			=> $strCurPhotoURLRaw, 
													'video_url'				=> $strCurVideoURL, 
													'video_id'				=> $strCurVideoID, 
													'video_thumb_small'		=> $strCurVideoThumbSmall, 
													'video_thumb_large'		=> $strCurVideoThumbLarge, 
													'votes'					=> number_format(($intCurVoteCount + $intCurVoteBonusCount), 0)
												);
			} else {
				// already approved/accepted
				$arrReturnData['status']			= self::ENTRANT_STATUS_ACCEPTED;
			}
		}
		unset($queryGetEntrantMeta);
		
		return $arrReturnData;
	}
	
	public function search_entrants() {
		$arrData						= array(	'paging' => array('max' => 0, 'cur' => 0, 'per' => 10), 
													'params' => array('firstname' => '', 'lastname' => '', 'city' => '', 'state' => ''), 
													'data' => array());
		$strSearchFirstName				= '';
		$strSearchLastName				= '';
		$strSearchCity					= '';
		$strSearchState					= '';
		$strAdditionalQuery				= '';
		$intItemsPerPage				= 10;
		$intItemsTotal					= 0;
		$intPagesCurrent				= 1;
		$intPagesTotal					= 0;
		
		if ((isset($_REQUEST['ihpp'])) && (intval($_REQUEST['ihpp']))) {
			$intItemsPerPage				= intval($_REQUEST['ihpp']);
		}
		if ((isset($_REQUEST['icp'])) && (intval($_REQUEST['icp']))) {
			$intPagesCurrent				= intval($_REQUEST['icp']);
		}
		if ((isset($_REQUEST['shfn'])) && (strlen($_REQUEST['shfn']))) {
			$strSearchFirstName				= stripslashes($_REQUEST['shfn']);
			$arrData['params']['firstname']	= $strSearchFirstName;
			
			$strAdditionalQuery				.=	sprintf(	" AND m.strHeroFirstName = '%s' ", 
															$this->db->escape($strSearchFirstName));
		}
		if ((isset($_REQUEST['shln'])) && (strlen($_REQUEST['shln']))) {
			$strSearchLastName				= stripslashes($_REQUEST['shln']);
			$arrData['params']['lastname']	= $strSearchLastName;

			$strAdditionalQuery				.=	sprintf(	" AND m.strHeroLastName = '%s' ", 
															$this->db->escape($strSearchLastName));
		}
		if ((isset($_REQUEST['shc'])) && (strlen($_REQUEST['shc']))) {
			$strSearchCity					= stripslashes($_REQUEST['shc']);
			$arrData['params']['city']		= $strSearchCity;

			$strAdditionalQuery				.=	sprintf(	" AND m.strHeroCity LIKE '%%%s%%' ", 
															$this->db->escape($strSearchCity));
		}
		if ((isset($_REQUEST['shs'])) && (strlen($_REQUEST['shs']))) {
			$strSearchState					= stripslashes($_REQUEST['shs']);
			$arrData['params']['state']		= $strSearchState;

			$strAdditionalQuery				.=	sprintf(	" AND m.strHeroState LIKE '%%%s%%' ", 
															$this->db->escape($strSearchState));
		}

		$strQuery						= sprintf(	"	SELECT COUNT(*) 
														FROM %sposts p 
														LEFT JOIN %snmam_entrant_meta m ON p.ID = m.intEntrantID 
														WHERE p.post_type = '%s' 
														AND p.post_status = 'publish' 
														%s", 
													$this->db->prefix, 
													$this->db->prefix, 
													self::ENTRANT_POSTTYPE_SLUG, 
													$strAdditionalQuery);
		$intItemsTotal					= $this->db->get_var($strQuery);
		
		if ($intItemsTotal > 0) {
			$intPagesTotal					= ceil($intItemsTotal / $intItemsPerPage);
			
			if ($intPagesCurrent > $intPagesTotal) {
				$intPagesCurrent				= $intPagesTotal;
			}
			if ($intPagesCurrent < 1) {
				$intPagesCurrent				= 1;
			}
			
			$arrData['paging']				= array('max' => $intPagesTotal, 'cur' => $intPagesCurrent, 'per' => $intItemsPerPage);

			$strQuery						= sprintf(	"	SELECT p.ID, m.strHeroFirstName, m.strHeroLastName, m.strHeroCity, m.strHeroState, m.boolHeroSemifinalist 
															FROM %sposts p 
															LEFT JOIN %snmam_entrant_meta m ON p.ID = m.intEntrantID 
															WHERE p.post_type = '%s' 
															AND p.post_status = 'publish' 
															%s 
															ORDER BY p.post_date DESC  
															LIMIT %d, %d", 
														$this->db->prefix, 
														$this->db->prefix, 
														self::ENTRANT_POSTTYPE_SLUG, 
														$strAdditionalQuery, 
														($intPagesCurrent - 1) * $intItemsPerPage, 
														$intItemsPerPage);
			$arrData['data']				= $this->db->get_results($strQuery);
		}
		
		return $arrData;
	}
	
	/*	
	==========================================================================
	CONTEST ENTRY SUBMISSION AND NOMINATION ACCEPTANCE METHODS
	==========================================================================	
	*/
	public function submit_contest_entry() {
		if ((isset($_POST['action'])) && 
			(strtolower($_POST['action']) == 'enter') && 
			(isset($_POST['strFirstName'])) && 
			(isset($_POST['strLastName'])) && 
			(isset($_POST['strEmail'])) && 
			(isset($_POST['strAddress'])) && 
			(isset($_POST['strCity'])) && 
			(isset($_POST['strState'])) && 
			(isset($_POST['strZIP'])) && 
			(isset($_POST['strPhone'])) && 
			(isset($_POST['strPromoCode'])) && 
			(isset($_POST['strPhotoFilename'])) && 
			(isset($_POST['strVideoURL'])) && 
			(isset($_POST['strVideoPhotoSmallURL'])) && 
			(isset($_POST['strVideoPhotoLargeURL'])) && 
			(isset($_POST['chkRulesAgree'])) && 
			(strlen($_POST['strFirstName'])) && 
			(strlen($_POST['strLastName'])) && 
			(strlen($_POST['strCity'])) && 
			(strlen($_POST['strState']))) {
			$boolCurIsApproved			= 1;
			$strCurFirstName			= trim(ucfirst(stripslashes($_POST['strFirstName'])));
			$strCurLastName				= trim(ucfirst(stripslashes($_POST['strLastName'])));
			$strCurTitle				= '';
			$strCurEmail				= trim(stripslashes($_POST['strEmail']));
			$strCurAddress				= trim(stripslashes($_POST['strAddress']));
			$strCurCity					= trim(ucfirst(stripslashes($_POST['strCity'])));
			$strCurState				= trim(ucfirst(stripslashes($_POST['strState'])));
			$strCurZIP					= trim(stripslashes($_POST['strZIP']));
			$strCurPhone				= trim(stripslashes($_POST['strPhone']));
			$strCurPromoCode			= trim(stripslashes($_POST['strPromoCode']));
			$intCurMediaType			= 0;
			$strCurPhotoFilename		= stripslashes($_POST['strPhotoFilename']);
			$strCurVideoURL				= stripslashes($_POST['strVideoURL']);
			$strCurVideoThumbSmall		= stripslashes($_POST['strVideoPhotoSmallURL']);
			$strCurVideoThumbLarge		= stripslashes($_POST['strVideoPhotoLargeURL']);
			$txtCurStory				= '';
			
			if (isset($_POST['txtStory'])) {
				$txtCurStory				= $this->_capitalize_lines(strip_tags(stripslashes($_POST['txtStory'])));
				
				if ($this->_count_words($txtCurStory) > 400) {
					header('Location: story-too-long/' . ((isset($_GET['facebook'])) ? '?facebook' : ''));
					die();
				}
			}
			
			if (isset($_POST['intMediaType'])) {
				$intCurMediaType			= intval($_POST['intMediaType']);
				
				switch ($intCurMediaType) {
					case self::ENTRANT_MEDIA_TYPE_PHOTO : 
						// photo
						$strCurVideoURL			= '';
						$strCurVideoThumbSmall	= '';
						$strCurVideoThumbLarge	= '';
						break;
						
					case self::ENTRANT_MEDIA_TYPE_VIDEO : 
						// video
						$txtCurStory			= '';
						$strCurPhotoFilename	= '';
						break;
				}
			}
			
			if ((!isset($_POST['chkHeroInfoIsSame'])) && 
				(isset($_POST['strHeroFirstName'])) && 
				(isset($_POST['strHeroLastName'])) && 
				(isset($_POST['strHeroEmail'])) && 
				(isset($_POST['strHeroAddress'])) && 
				(isset($_POST['strHeroCity'])) && 
				(isset($_POST['strHeroState'])) && 
				(isset($_POST['strHeroZIP'])) && 
				(isset($_POST['strHeroPhone']))) {
				$boolCurIsApproved			= 0;
				$strCurHeroFirstName		= trim(ucfirst(stripslashes($_POST['strHeroFirstName'])));
				$strCurHeroLastName			= trim(ucfirst(stripslashes($_POST['strHeroLastName'])));
				$strCurHeroEmail			= trim(stripslashes($_POST['strHeroEmail']));
				$strCurHeroAddress			= trim(stripslashes($_POST['strHeroAddress']));
				$strCurHeroCity				= trim(ucfirst(stripslashes($_POST['strHeroCity'])));
				$strCurHeroState			= trim(ucfirst(stripslashes($_POST['strHeroState'])));
				$strCurHeroZIP				= trim(stripslashes($_POST['strHeroZIP']));
				$strCurHeroPhone			= trim(stripslashes($_POST['strHeroPhone']));
			} else {
				$strCurHeroFirstName		= $strCurFirstName;
				$strCurHeroLastName			= $strCurLastName;
				$strCurHeroEmail			= $strCurEmail;
				$strCurHeroAddress			= $strCurAddress;
				$strCurHeroCity				= $strCurCity;
				$strCurHeroState			= $strCurState;
				$strCurHeroZIP				= $strCurZIP;
				$strCurHeroPhone			= $strCurPhone;
			}

			$strCurTitle				= $strCurHeroFirstName . ' ' . $strCurHeroLastName . ' - ' . $strCurHeroCity . ', ' . $strCurHeroState;

			$strQuery					=	sprintf(	"SELECT intEntrantID FROM %snmam_entrant_meta WHERE strHeroFirstName = '%s' AND strHeroLastName = '%s' AND strHeroCity = '%s' AND strHeroState = '%s' AND intEntrantID IN (SELECT ID FROM %sposts WHERE post_type = '%s' AND ((post_status = 'private') OR (post_status = 'publish')))", 
														$this->db->prefix, 
														$this->db->escape($strCurHeroFirstName), 
														$this->db->escape($strCurHeroLastName), 
														$this->db->escape($strCurHeroCity), 
														$this->db->escape($strCurHeroState), 
														$this->db->prefix, 
														self::ENTRANT_POSTTYPE_SLUG);
			$queryDupeCheck				= $this->db->get_results($strQuery);
			
			if (count($queryDupeCheck) == 0) {
				$arrPostOptions				=	array(
													'post_type'				=> self::ENTRANT_POSTTYPE_SLUG, 
													'post_title'			=> $strCurTitle, 
													'post_content'			=> $txtCurStory, 
													'post_status'			=> 'private', 
													'post_author'			=> 1, 
													'post_date'				=> date('Y-m-d H:i:s'), 
													'post_date_gmt'			=> gmdate('Y-m-d H:i:s', time() + (3600 * self::VOTING_TIMEZONE)), 
													'post_excerpt'			=> '', 
													'comment_status'		=> 'open'
												);
				$intCurPostID				= wp_insert_post($arrPostOptions);
				
				if ($intCurPostID > 0) {
					// set featured photo on entry post
					$this->_set_entry_photo($intCurPostID, $strCurTitle, $intCurMediaType, $strCurPhotoFilename, $strCurVideoThumbLarge);

					// save metadata
					$strQuery					= sprintf(	"	INSERT INTO		%snmam_entrant_meta 
																				(	intEntrantID, 
																					boolApproved, 
																					strFirstName, 
																					strLastName, 
																					strEmail, 
																					strAddress, 
																					strCity, 
																					strState, 
																					strZIP, 
																					strPhone, 
																					strHeroFirstName, 
																					strHeroLastName, 
																					strHeroEmail, 
																					strHeroAddress, 
																					strHeroCity, 
																					strHeroState, 
																					strHeroZIP, 
																					strHeroPhone, 
																					strVideoURL, 
																					strVideoThumbSmall, 
																					strVideoThumbLarge, 
																					strPromoCode, 
																					strEnteredIP, 
																					dtEntered)
																				VALUES
																				(
																					%d, 
																					%d, 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					'%s', 
																					NOW()
																				)", 
																				$this->db->prefix, 
																				intval($intCurPostID), 
																				intval($boolCurIsApproved), 
																				$this->db->escape($strCurFirstName), 
																				$this->db->escape($strCurLastName), 
																				$this->db->escape($strCurEmail), 
																				$this->db->escape($strCurAddress), 
																				$this->db->escape($strCurCity), 
																				$this->db->escape($strCurState), 
																				$this->db->escape($strCurZIP), 
																				$this->db->escape($strCurPhone), 
																				$this->db->escape($strCurHeroFirstName), 
																				$this->db->escape($strCurHeroLastName), 
																				$this->db->escape($strCurHeroEmail), 
																				$this->db->escape($strCurHeroAddress), 
																				$this->db->escape($strCurHeroCity), 
																				$this->db->escape($strCurHeroState), 
																				$this->db->escape($strCurHeroZIP), 
																				$this->db->escape($strCurHeroPhone), 
																				$this->db->escape($strCurVideoURL), 
																				$this->db->escape($strCurVideoThumbSmall), 
																				$this->db->escape($strCurVideoThumbLarge), 
																				$this->db->escape($strCurPromoCode), 
																				$this->db->escape($_SERVER['REMOTE_ADDR']));
					$this->db->query($strQuery);
				}
				
				// send e-mail
				if ($boolCurIsApproved) {
					$this->send_admin_submission_email($strCurHeroFirstName, $strCurHeroLastName, $strCurHeroCity, $strCurHeroState);
				} else {
					// this entry needs to be approved by the nominated hero first, so let's generate keys for access to the preview page for this entry and fire off an e-mail to the hero
					$arrKeyData				= $this->_generate_nominated_approval_keys($intCurPostID, $strCurHeroEmail);
					
					$this->send_nominated_hero_email($intCurPostID, $strCurHeroEmail, $strCurHeroFirstName, $strCurHeroLastName, $strCurFirstName, $strCurLastName, $arrKeyData['key'], $arrKeyData['keyed_email']);
				}
				
				// redirect to success page
				header('Location: success/' . ((isset($_GET['facebook'])) ? '?facebook' : ''));
				die();
			} else {
				header('Location: duplicate/' . ((isset($_GET['facebook'])) ? '?facebook' : ''));
				die();
			}
			unset($queryDupeCheck);
		} else {
			header('Location: error/' . ((isset($_GET['facebook'])) ? '?facebook' : ''));
			die();
		}
		
		header('Location: error/' . ((isset($_GET['facebook'])) ? '?facebook' : ''));
		die();
	}

	public function accept_nomination_entry() {
		if ((isset($_REQUEST['ki'])) && (isset($_REQUEST['ke']))) {
			$intEntrantID			= $this->verify_nominated_approval_keys($_REQUEST['ki'], $_REQUEST['ke']);
			
			if ($intEntrantID > 0) {
				if ((isset($_POST['action'])) && 
					(strtolower($_POST['action']) == 'accept') && 
					(isset($_POST['strHeroFirstName'])) && 
					(isset($_POST['strHeroLastName'])) && 
					(isset($_POST['strHeroEmail'])) && 
					(isset($_POST['strHeroAddress'])) && 
					(isset($_POST['strHeroCity'])) && 
					(isset($_POST['strHeroState'])) && 
					(isset($_POST['strHeroZIP'])) && 
					(isset($_POST['strHeroPhone'])) && 
					(isset($_POST['strPhotoFilename'])) && 
					(isset($_POST['strVideoURL'])) && 
					(isset($_POST['strVideoPhotoSmallURL'])) && 
					(isset($_POST['strVideoPhotoLargeURL'])) && 
					(isset($_POST['txtStory'])) && 
					(isset($_POST['chkRulesAgree'])) && 
					(strlen($_POST['strHeroFirstName'])) && 
					(strlen($_POST['strHeroLastName'])) && 
					(strlen($_POST['strHeroCity'])) && 
					(strlen($_POST['strHeroState']))) {
					$strCurHeroFirstName		= ucfirst(stripslashes($_POST['strHeroFirstName']));
					$strCurHeroLastName			= ucfirst(stripslashes($_POST['strHeroLastName']));
					$strCurHeroEmail			= stripslashes($_POST['strHeroEmail']);
					$strCurHeroAddress			= stripslashes($_POST['strHeroAddress']);
					$strCurHeroCity				= ucfirst(stripslashes($_POST['strHeroCity']));
					$strCurHeroState			= ucfirst(stripslashes($_POST['strHeroState']));
					$strCurHeroZIP				= stripslashes($_POST['strHeroZIP']);
					$strCurHeroPhone			= stripslashes($_POST['strHeroPhone']);
					$txtCurStory				= '';
					$strCurTitle				= $strCurHeroFirstName . ' ' . $strCurHeroLastName . ' - ' . $strCurHeroCity . ', ' . $strCurHeroState;
					
					if (isset($_POST['txtStory'])) {
						$txtCurStory				= $this->_capitalize_lines(strip_tags(stripslashes($_POST['txtStory'])));
					}
					
					$strQuery					= sprintf(	"	UPDATE	%snmam_entrant_meta 
																SET		boolApproved				= 1, 
																		strHeroFirstName			= '%s', 
																		strHeroLastName				= '%s', 
																		strHeroEmail				= '%s', 
																		strHeroAddress				= '%s', 
																		strHeroCity					= '%s', 
																		strHeroState				= '%s', 
																		strHeroZIP					= '%s', 
																		strHeroPhone				= '%s' 
																WHERE	intEntrantID				= %d", 
															$this->db->prefix, 
															$this->db->escape($strCurHeroFirstName), 
															$this->db->escape($strCurHeroLastName), 
															$this->db->escape($strCurHeroEmail), 
															$this->db->escape($strCurHeroAddress), 
															$this->db->escape($strCurHeroCity), 
															$this->db->escape($strCurHeroState), 
															$this->db->escape($strCurHeroZIP), 
															$this->db->escape($strCurHeroPhone), 
															intval($intEntrantID));
					$this->db->query($strQuery);
					
					// update post with new story text and title
					wp_update_post(array('ID' => $intEntrantID, 'post_title' => $strCurTitle, 'post_content' => $txtCurStory));
					
					// see if we need to update any media on this entry
					$boolCurMediaNeedsUpdate	= false;
					$intCurMediaType			= 0;
					$strCurPhotoFilename		= stripslashes($_POST['strPhotoFilename']);
					$strCurVideoURL				= stripslashes($_POST['strVideoURL']);
					$strCurVideoThumbSmall		= stripslashes($_POST['strVideoPhotoSmallURL']);
					$strCurVideoThumbLarge		= stripslashes($_POST['strVideoPhotoLargeURL']);
					
					if (isset($_POST['intMediaType'])) {
						$intCurMediaType			= intval($_POST['intMediaType']);
						
						switch ($intCurMediaType) {
							case self::ENTRANT_MEDIA_TYPE_PHOTO : 
								// photo
								if (strlen($strCurPhotoFilename)) {
									$boolCurMediaNeedsUpdate	= true;
									$strCurVideoURL				= '';
									$strCurVideoThumbSmall		= '';
									$strCurVideoThumbLarge		= '';
								}
								break;
								
							case self::ENTRANT_MEDIA_TYPE_VIDEO : 
								// video
								if ((strlen($strCurVideoURL))  && (strlen($strCurVideoThumbSmall)) && (strlen($strCurVideoThumbLarge))) {
									$boolCurMediaNeedsUpdate	= true;
									$strCurPhotoFilename		= '';
								}
								break;
						}
						
						if ($boolCurMediaNeedsUpdate) {
							$strQuery					= sprintf(	"	UPDATE	%snmam_entrant_meta 
																		SET		strVideoURL				= '%s', 
																				strVideoThumbSmall		= '%s', 
																				strVideoThumbLarge		= '%s' 
																		WHERE	intEntrantID				= %d", 
																	$this->db->prefix, 
																	$this->db->escape($strCurVideoURL), 
																	$this->db->escape($strCurVideoThumbSmall), 
																	$this->db->escape($strCurVideoThumbLarge), 
																	intval($intEntrantID));
							$this->db->query($strQuery);
							
							// save photo and attach is as a featured post to this entry
							$this->_set_entry_photo($intEntrantID, $strCurTitle, $intCurMediaType, $strCurPhotoFilename, $strCurVideoThumbLarge);
						}
					}
					
					// send admin e-mail
					$this->send_admin_submission_email($strCurHeroFirstName, $strCurHeroLastName, $strCurHeroCity, $strCurHeroState);
					
					// redirect to success page
					header('Location: ./success/');
					die();
				} else {
					header('Location: ./error/');
					die();
				}
			} else {
				header('Location: ./error/');
				die();
			}
		} else {
			header('Location: ./error/');
			die();
		}
	}

	public function decline_nomination_entry() {
		if ((isset($_REQUEST['ki'])) && (isset($_REQUEST['ke']))) {
			$intEntrantID			= $this->verify_nominated_approval_keys($_REQUEST['ki'], $_REQUEST['ke']);
			
			if ($intEntrantID > 0) {
				wp_delete_post($intEntrantID, true);
				
				$strQuery				= sprintf(	"DELETE FROM %snmam_entrant_meta WHERE intEntrantID = %d LIMIT 1", 
													$this->db->prefix, 
													intval($intEntrantID));
				$this->db->query($strQuery);
				
				header('Location: ./success/');
				die();
			} else {
				header('Location: ./error/');
				die();
			}
		} else {
			header('Location: ./error/');
			die();
		}
	}
	
	/*	
	==========================================================================
	CAPTCHA METHODS
	==========================================================================	
	*/
	public function verify_math_captcha($strIPAddress, $intQuestionID, $intAnswer) {
		$boolResult			= false;
		
		if ($intQuestionID > 0) {
			$strQuery					= sprintf(	"SELECT intResult FROM %snmam_captcha_log WHERE intID = %d AND boolExpired = 0 LIMIT 1", 
													$this->db->prefix, 
													intval($intQuestionID));
			$queryGetQuestion			= $this->db->get_results($strQuery);
			
			if (count($queryGetQuestion)) {
				$objRow						= $queryGetQuestion[0];
				
				$intResult					= intval($objRow->intResult);
				
				if (intval($intAnswer) == intval($intResult)) {
					$boolResult				= true;
				}

				$strQuery				= sprintf(	"UPDATE %snmam_captcha_log SET intUserAnswer = %d, boolExpired = 1, boolAnswered = %d WHERE intID = %d", 
													$this->db->prefix, 
													intval($intAnswer), 
													(($boolResult) ? 1 : 0), 
													intval($intQuestionID));
				$this->db->query($strQuery);
			}
		}
		
		return $boolResult;
	}

	public function generate_math_captcha($strIPAddress, $boolCheckForExisting = true) {
		$boolHasExistingQuestion	= false;
		$intQuestionID				= 0;
		$intNumber1					= 0;
		$intNumber2					= 0;
		$strProblemText				= '';
		
		if ($boolCheckForExisting) {
			$strQuery					= sprintf(	"SELECT intID, intNumber1, intNumber2 FROM %snmam_captcha_log WHERE strIPAddress = '%s' AND boolExpired = 0 LIMIT 1", 
													$this->db->prefix, 
													$this->db->escape($strIPAddress));
			$queryGetQuestion			= $this->db->get_results($strQuery);
			
			if (count($queryGetQuestion)) {
				$boolHasExistingQuestion	= true;
			}
		}
		
		if ($boolHasExistingQuestion) {
			$objRow					= $queryGetQuestion[0];
			$intQuestionID			= intval($objRow->intID);
			$intNumber1				= intval($objRow->intNumber1);
			$intNumber2				= intval($objRow->intNumber2);
			$strProblemText			= $intNumber1 . ' plus ' . $intNumber2;
		} else {
			$intNumber1				= mt_rand(1, 10);
			$intNumber2				= mt_rand(1, 10);
			$strProblemText			= '';
			
			// addition
			if ($intNumber1 === $intNumber2) {
				// numbers are equal to each other (making the captcha too easy)
				return ($this->generate_math_captcha($strIPAddress, false));
			} else {
				$intResult					= ($intNumber1 + $intNumber2);
				$strProblemText				= $intNumber1 . ' plus ' . $intNumber2;
				
				// save it
				$strQuery					= sprintf(	"INSERT INTO %snmam_captcha_log (intNumber1, intNumber2, intResult, intUserAnswer, boolAnswered, boolExpired, strIPAddress) VALUES (%d, %d, %d, NULL, 0, 0, '%s')", 
														$this->db->prefix, 
														intval($intNumber1), 
														intval($intNumber2), 
														intval($intNumber1 + $intNumber2), 
														$this->db->escape($strIPAddress));
				$this->db->query($strQuery);
				
				$intQuestionID				= $this->db->insert_id;
			}
		}
		
		return array('id' => $intQuestionID, 'number1' => $intNumber1, 'number2' => $intNumber2, 'problem' => $strProblemText);
	}

	/*	
	==========================================================================
	VOTING METHODS
	==========================================================================	
	*/
	private function _get_bonus_votes($intEntrantID) {
		$intBonusVotes			= 0;
		
		$strQuery					= sprintf(	"SELECT strPromoCode FROM %snmam_entrant_meta WHERE intEntrantID = %d AND LENGTH(strPromoCode) > 0 AND strPromoCode IN (SELECT strCode FROM %snmam_dealers) LIMIT 1", 
												$this->db->prefix, 
												intval($intEntrantID), 
												$this->db->prefix);
		$queryGetPromoCode			= $this->db->get_results($strQuery);
		
		if (count($queryGetPromoCode)) {
			$intBonusVotes			= 10;
		}
		
		unset($queryGetPromoCode);
		
		return $intBonusVotes;
	}
	
	public function has_voted_today($strIPAddress) {
		$boolHasVoted			= true;
		
		$strQuery				= sprintf(	"SELECT intID FROM %snmam_entrant_votes WHERE strIPAddress = '%s' AND dtDate = '%s' LIMIT 1",  
											$this->db->prefix, 
											$this->db->escape($strIPAddress), 
											$this->db->escape(gmdate('Y-m-d', time() + (3600 * self::VOTING_TIMEZONE))));
		$queryCheckVote			= $this->db->get_results($strQuery);
		
		if ((is_array($queryCheckVote)) && (count($queryCheckVote) === 0)) {
			$boolHasVoted			= false;
		}
		
		unset($queryCheckVote);
		
		return $boolHasVoted;
	}
	
	public function get_bonus_question_status($strIPAddress) {
		$intStatusCode			= -1;
		
		if (strlen($strIPAddress)) {
			$strQuery				= sprintf(	"SELECT boolSpent FROM %snmam_entrant_votes_extra WHERE strIPAddress = '%s' AND dtDate = '%s' LIMIT 1",  
												$this->db->prefix, 
												$this->db->escape($strIPAddress), 
												$this->db->escape(gmdate('Y-m-d', time() + (3600 * self::VOTING_TIMEZONE))));
			$queryGetStatus			= $this->db->get_results($strQuery);
			
			if (count($queryGetStatus)) {
				$objRow					= $queryGetStatus[0];
				$intCurStatus			= intval($objRow->boolSpent);
				
				if ($intCurStatus == 0) {
					$intStatusCode			= self::EXTRAVOTE_STATUS_EARNED;
				} else if ($intCurStatus == 1) {
					$intStatusCode			= self::EXTRAVOTE_STATUS_SPENT;
				}
			}
			unset($queryGetStatus);
		}
		
		return $intStatusCode;
	}
	
	public function has_bonus_vote_today($strIPAddress) {
		$boolReturnValue		= false;
		
		$strQuery				= sprintf(	"SELECT boolSpent FROM %snmam_entrant_votes_extra WHERE strIPAddress = '%s' AND dtDate = '%s' LIMIT 1",  
											$this->db->prefix, 
											$this->db->escape($strIPAddress), 
											$this->db->escape(gmdate('Y-m-d', time() + (3600 * self::VOTING_TIMEZONE))));
		$queryGetRecord			= $this->db->get_results($strQuery);
		
		if ((is_array($queryGetRecord)) && (count($queryGetRecord))) {
			$boolReturnValue		= true;
		} else {
			$boolReturnValue		= false;
		}
		unset($queryGetRecord);
		
		return $boolReturnValue;
	}


	public function check_bonus_vote_status($strIPAddress, $intType = self::EXTRAVOTE_STATUS_ANY) {
		$boolReturnValue		= false;
		
		$strQuery				= sprintf(	"SELECT boolSpent FROM %snmam_entrant_votes_extra WHERE strIPAddress = '%s' AND dtDate = '%s' LIMIT 1",  
											$this->db->prefix, 
											$this->db->escape($strIPAddress), 
											$this->db->escape(gmdate('Y-m-d', time() + (3600 * self::VOTING_TIMEZONE))));
		$queryGetRecord			= $this->db->get_results($strQuery);
		
		if ((is_array($queryGetRecord)) && (count($queryGetRecord))) {
			$objRow					= $queryGetRecord[0];
			$boolSpentExtraVote		= intval($objRow->boolSpent);
			
			switch ($intType) {
				case self::EXTRAVOTE_STATUS_ANY : 
					// there is a answer record, of some type, for today
					$boolReturnValue		= true;
					break;
					
				case self::EXTRAVOTE_STATUS_EARNED : 
					// answered today, but not used today
					if ($boolSpentExtraVote === 0) {
						$boolReturnValue		= true;
					} else {
						$boolReturnValue		= false;
					}
					break;
					
				case self::EXTRAVOTE_STATUS_SPENT : 
					// answered and used
					if ($boolSpentExtraVote === 1) {
						$boolReturnValue		= true;
					} else {
						$boolReturnValue		= false;
					}
					break;
			}
		} else {
			$boolReturnValue		= false;
		}
		unset($queryGetRecord);
		
		return $boolReturnValue;
	}

	public function record_entrant_vote() {
		if ((isset($_POST['action'])) && 
			(strtolower($_POST['action']) == 'vote') && 
			(isset($_SERVER['REMOTE_ADDR'])) && 
			(isset($_POST['strVoteToken'])) && 
			(isset($_POST['intEntrantID'])) && 
			(isset($_POST['strFirstName'])) && 
			(isset($_POST['strLastName'])) && 
			(isset($_POST['strEmail'])) && 
			(isset($_POST['intCAPTCHAID'])) && 
			(isset($_POST['strCAPTCHA'])) && 
			(isset($_POST['chkRulesAgree'])) && 
			(strlen($_SERVER['REMOTE_ADDR'])) && 
			(strlen($_POST['strVoteToken'])) && 
			(intval($_POST['intEntrantID'])) && 
			(strlen($_POST['strFirstName'])) && 
			(strlen($_POST['strLastName'])) && 
			(strlen($_POST['strEmail'])) && 
			(intval($_POST['intCAPTCHAID'])) && 
			(wp_verify_nonce($_POST['strVoteToken'], self::NONCE_VOTING_TOKEN))) {

			// verify math captcha
			if ((intval($_POST['strCAPTCHA']) > 0) && ($this->verify_math_captcha($_SERVER['REMOTE_ADDR'], intval($_POST['intCAPTCHAID']), intval($_POST['strCAPTCHA'])))) {
				$strCurIPAddress	= $_SERVER['REMOTE_ADDR'];
				$intCurEntrantID	= intval($_POST['intEntrantID']);
				$strCurFirstName	= stripslashes($_POST['strFirstName']);
				$strCurLastName		= stripslashes($_POST['strLastName']);
				$strCurEmail		= stripslashes($_POST['strEmail']);
				$boolCurSubscribe	= ((isset($_POST['chkTermsAgree'])) ? true : false);
				$arrAdditionalInfo	= array();
				$intCurVotes		= 1;
				$boolCurBonusVote	= false;
				
				foreach ($_POST as $strFormKey => $strFormValue) {
					if (strpos($strFormKey, 'chkIdent_') !== false) {
						if ((stripos($strFormValue, 'other') !== false) && (isset($_POST['strIdent_Other'])) && (strlen($_POST['strIdent_Other']))) {
							$strFormValue			= 'Other (' . $_POST['strIdent_Other'] . ')';
						}
						array_push($arrAdditionalInfo, $strFormValue);
					}
				}
				
				// dupe check
				if (!$this->has_voted_today($strCurIPAddress)) {
					// do we have a bonus vote, and if so, have we spent it?
					if (($this->has_bonus_vote_today($strCurIPAddress)) && (!$this->check_bonus_vote_status($strCurIPAddress, self::EXTRAVOTE_STATUS_SPENT))) {
						$boolCurBonusVote	= true;
						$intCurVotes		= 2;
						
						$strQuery			= sprintf(	"UPDATE %snmam_entrant_votes_extra SET boolSpent = 1 WHERE strIPAddress = '%s' AND dtDate = '%s'", 
														$this->db->prefix, 
														$this->db->escape($strCurIPAddress), 
														$this->db->escape(gmdate('Y-m-d', time() + (3600 * self::VOTING_TIMEZONE))));
						$this->db->query($strQuery);
					}
				
					// do it!
					$strQuery			= sprintf(	"INSERT INTO %snmam_entrant_votes (intEntrantID, dtDate, strIPAddress, strFirstName, strLastName, strEmail, txtAdditionalInfo, boolSubscribe, intVotes, boolHadBonus) VALUES (%d, '%s', '%s', '%s', '%s', '%s', '%s', %d, %d, %d)", 
													$this->db->prefix, 
													intval($intCurEntrantID), 
													$this->db->escape(gmdate('Y-m-d', time() + (3600 * self::VOTING_TIMEZONE))), 
													$this->db->escape($strCurIPAddress), 
													$this->db->escape($strCurFirstName), 
													$this->db->escape($strCurLastName), 
													$this->db->escape($strCurEmail), 
													$this->db->escape(serialize($arrAdditionalInfo)), 
													intval($boolCurSubscribe), 
													intval($intCurVotes), 
													intval($boolCurBonusVote));
					$this->db->query($strQuery);
					
					/*
					// send thank-you e-mail
					$strQuery			= sprintf(	"SELECT post_title, post_content FROM %sposts WHERE ID = (SELECT post_id FROM %spostmeta WHERE meta_key = 'email_template_type' AND meta_value = 'voting-thanks') LIMIT 1", 
													$this->db->prefix, 
													$this->db->prefix);
					$queryGetEmail		= $this->db->get_results($strQuery);
					
					if (count($queryGetEmail)) {
						$objRow				= $queryGetEmail[0];
						
						$strMailSubject		= stripslashes($objRow->post_title);
						$txtMailMessage		= wpautop(stripslashes($objRow->post_content));
						
						$arrMailHeaders		= array('From: NMEDA.com Mobility Awareness Month Voting <info@mobilityawarenessmonth.com>');
						
						if (filter_var($strCurEmail, FILTER_VALIDATE_EMAIL)) {
							wp_mail(	$strCurEmail, 
										$strMailSubject, 
										$txtMailMessage, 
										$arrMailHeaders);
						}
					}
					unset($queryGetEmail);
					*/
													
					// update vote count
					if (self::REALTIME_VOTE_COUNTS) {
						$intCurVoteCount	= intval(get_post_meta($intCurEntrantID, self::ENTRANT_META_VOTES, true)) + $intCurVotes;
						update_post_meta($intCurEntrantID, self::ENTRANT_META_VOTES, $intCurVoteCount);
					}
					
					header('Location: thanks/');
					die();
				} else {
					header('Location: already-voted/');
					die();
				}
			} else {
				header('Location: captcha-incorrect/?pid=' . intval($_POST['intEntrantID']));
				die();
			}
		} else {
			if ((isset($_SERVER['HTTP_REFERER'])) && (strlen($_SERVER['HTTP_REFERER']))) {
				header('Location: ' . $_SERVER['HTTP_REFERER']);
			} else {
				header('Location: /local-heroes/');
			}
			die();
		}
	}

	/*	
	==========================================================================
	SEND MAIL
	==========================================================================	
	*/
	public function send_admin_submission_email($strFirstName, $strLastName, $strCity, $strState) {
		$txtMailMessage				=	'<h1>A new contest entrant has been submitted to the site!</h1>' . 
										'<p>' . 
											'<strong>NAME:</strong> ' . $strFirstName . ' ' . $strLastName . '<br />' . 
											'<strong>CITY:</strong> ' . $strCity . '<br />' . 
											'<strong>STATE:</strong> ' . $strState . '<br />' . 
										'</p>' . 
										'<p>Visit the NMEDA Mobility Awareness Month WordPress administration area (<a href="http://www.mobilityawarenessmonth.com/wp-admin/">http://www.mobilityawarenessmonth.com/wp-admin/</a>) to view and approve this new entrant.</p>';
		$arrMailHeaders				= array('From: NMEDA.com Contest Submission System <info@mobilityawarenessmonth.com>');

		if (filter_var(self::ENTRANT_ADMIN_EMAIL, FILTER_VALIDATE_EMAIL)) {
			wp_mail(	self::ENTRANT_ADMIN_EMAIL, 
						'NMEDA NMAM - New Contest Entrant Awaiting Approval!', 
						$txtMailMessage, 
						$arrMailHeaders);
		}
	}

	public function send_nominated_hero_email($intEntryID, $strHeroEmail, $strHeroFirstName, $strHeroLastName, $strSubmittorFirstName, $strSubmittorLastName, $strKey, $strKeyedEmail) {
		$strHeroPreviewLink			=	'';
		
		// look up preview page URL
		$queryGetPreviewPage		= new WP_Query(array('post_type' => 'page', 'meta_key' => 'is_nominated_preview_page', 'meta_value' => 'true'));
		
		if ($queryGetPreviewPage->have_posts()) {
			$queryGetPreviewPage->the_post();
			
			$strHeroPreviewLink			= get_permalink();
			$strHeroPreviewLink			.=	sprintf(	"?ki=%s&ke=%s", 
														$strKey, 
														$strKeyedEmail);

			wp_reset_query();
			wp_reset_postdata();
			
			$txtMailMessage				=	'<p><strong>Greetings, ' . $strHeroFirstName . ' ' . $strHeroLastName . '!</strong></p>' . 
											'<p>A caregiver, friend or family member (' . $strSubmittorFirstName . ' ' . $strSubmittorLastName . ') has nominated you as their Local Hero in the second annual National Mobility Awareness Month contest. We will be giving away a minimum of three Wheelchair Accessible Vehicles to Local Heroes living active and mobile lifestyles.</p>' . 
											'<p><a href="' . $strHeroPreviewLink . '"><strong><u>In order to participate and have your video or story published to our website, we need your permission by clicking here</u></strong></a>.</p>' . 
											'<p>For more information about the program and for rules and regulations, please visit <a href="http://www.mobilityawarenessmonth.com/">www.MobilityAwarenessMonth.com</a>.</p>' . 
											'<p><strong>Thank you and best of luck to you.</strong></p>';
			$arrMailHeaders				= array('From: NMEDA.com Contest Submission System <info@mobilityawarenessmonth.com>');
	
			if (filter_var($strHeroEmail, FILTER_VALIDATE_EMAIL)) {
				wp_mail(	$strHeroEmail, 
							$strHeroFirstName . ', You\'ve Been Nominated at MobilityAwarenessMonth.com!', 
							$txtMailMessage, 
							$arrMailHeaders);
			}
		}
	}

	/*	
	==========================================================================
	MISCELLANEOUS UTILITY METHODS
	==========================================================================	
	*/
	private function _format_number($dblNumber) {
		return preg_replace("/[^0-9\.\-]+/", "", $dblNumber);
	}
	
	private function _get_youtube_video_id($strVideoURL) {
		$strVideoID					= '';
		$strRegExp					= "/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/";
		preg_match($strRegExp, $strVideoURL, $arrMatches);
		
		if ((isset($arrMatches)) && (count($arrMatches))) {
			$strVideoID					= $arrMatches[7];
		}
		
		return $strVideoID;
	}

	private function _set_entry_photo($intPostID, $strPostTitle, $intMediaType, $strPhotoFilename, $strVideoThumbLarge) {
		// add post thumbnail
		$arrUploadPath				= wp_upload_dir();
		$strUploadURL				= $arrUploadPath['url'];
		$strUploadPath				= $arrUploadPath['basedir'] . DIRECTORY_SEPARATOR . '/';

		$strPostSlug				= basename(get_permalink($intPostID));
		$strPostImageFilename		= 'entrant-' . $strPostSlug . '-id-' . $intPostID;
		$strFeaturedImageFilename	= '';
		
		switch ($intMediaType) {
			case self::ENTRANT_MEDIA_TYPE_PHOTO : 
				// photo
				if (strlen($strPhotoFilename)) {
					$arrPostImageInfo		= pathinfo($strPhotoFilename);
					$strPostImageExtension	= $arrPostImageInfo['extension'];
					$strPostImageFilename	.= '.' . $strPostImageExtension;
					
					copy(	$strUploadPath . $strPhotoFilename, 
							$strUploadPath . $strPostImageFilename);
							
					if (file_exists($strUploadPath . $strPostImageFilename)) {
						$strFeaturedImageFilename	= $strPostImageFilename;
					}
				}
				break;
				
			case self::ENTRANT_MEDIA_TYPE_VIDEO : 
				// video
				if (strlen($strVideoThumbLarge)) {
					$arrPostImageInfo		= pathinfo($strVideoThumbLarge);
					$strPostImageExtension	= $arrPostImageInfo['extension'];
					$strPostImageFilename	.= '.' . $strPostImageExtension;

					$objCURL				= curl_init();
					
					curl_setopt($objCURL,	CURLOPT_URL,			$strVideoThumbLarge);
					curl_setopt($objCURL,	CURLOPT_FOLLOWLOCATION,	1);
					curl_setopt($objCURL,	CURLOPT_USERAGENT,		'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17');
					curl_setopt($objCURL,	CURLOPT_RETURNTRANSFER,	1);
					curl_setopt($objCURL,	CURLOPT_HEADER,			0);
					
					$binData				= curl_exec($objCURL);
		
					if (!curl_errno($objCURL)) {
						$objHandle				= fopen($strUploadPath . $strPostImageFilename, 'w');
						
						if ($objHandle) {
							fwrite($objHandle, $binData);
							fclose($objHandle);
							
							if (file_exists($strUploadPath . $strPostImageFilename)) {
								$strFeaturedImageFilename	= $strPostImageFilename;
							}
						}
					} 
					curl_close($objCURL);
				}
				break;
		}
		
		if ((strlen($strFeaturedImageFilename)) && (file_exists($strUploadPath . $strFeaturedImageFilename))) {
			$strFeaturedImageFilename	= $strUploadPath . '/' . $strFeaturedImageFilename;
			
			$arrFiletype				= wp_check_filetype(basename($strFeaturedImageFilename));
			$arrAttachmentDetails		= 	array(
												'guid'				=> $strUploadURL . '/' . basename($strFeaturedImageFilename), 
												'post_mime_type'	=> $arrFiletype['type'], 
												'post_title'		=> 'Entry Photo for ' . $strPostTitle, 
												'post_content'		=> '', 
												'post_status'		=> 'inherit'
											);
			$intCurAttachmentID			= wp_insert_attachment($arrAttachmentDetails, $strFeaturedImageFilename, $intPostID);
			$arrAttachmentMetadata		= wp_generate_attachment_metadata($intCurAttachmentID, $strFeaturedImageFilename);
			
			wp_update_attachment_metadata($intCurAttachmentID, $arrAttachmentMetadata);
			
			delete_post_meta($intPostID, '_thumbnail_id');
			update_post_meta($intPostID, '_thumbnail_id', $intCurAttachmentID);
		}
	}
	
	private function _generate_nominated_approval_keys($intEntrantID, $strEmail) {
		$strSalt					= uniqid(mt_rand(), true);
		$strKey						= md5(uniqid() . $strSalt);
		$strKeyedEmail				= md5($strSalt . strtolower($strEmail) . $strKey);
		
		$strQuery					= sprintf(	"INSERT INTO %snmam_entrant_preview_keys (intEntrantID, strKey, strSalt, strKeyedEmail) VALUES (%d, '%s', '%s', '%s')", 
												$this->db->prefix, 
												intval($intEntrantID), 
												$this->db->escape($strKey), 
												$this->db->escape($strSalt), 
												$this->db->escape($strKeyedEmail));
		$this->db->query($strQuery);
		
		return array('salt' => $strSalt, 'key' => $strKey, 'keyed_email' => $strKeyedEmail);
	}
	
	private function _capitalize_lines($txtContent) {
		$txtReturn			= '';
		
		$arrLines			= explode("\n", $txtContent);
		
		foreach ($arrLines as $strLine) {
			$txtReturn			.=	ucfirst($strLine) . "\n";
		}
		
		return $txtReturn;
	}

	private function _count_words($txtContent) {
		$txtReturn			= '';
		
		$txtContent			= trim(preg_replace('{\s+}', ' ', $txtContent));
		$arrWords			= explode(' ', $txtContent);
		
		return count($arrWords);
	}
	
	private function _get_entrant_column_metadata() {
		$strQuery			= sprintf(	"	SELECT m.intEntrantID, m.strHeroFirstName, m.strHeroLastName, m.strHeroEmail, m.strHeroCity, m.strHeroState, m.boolApproved, p.post_status 
											FROM %snmam_entrant_meta m 
											LEFT JOIN %sposts p ON m.intEntrantID = p.ID 
											WHERE m.intEntrantID IN (SELECT ID FROM %sposts WHERE post_type = '%s' AND (post_status = 'private' OR post_status = 'publish'))", 
										$this->db->prefix, 
										$this->db->prefix, 
										$this->db->prefix, 
										self::ENTRANT_POSTTYPE_SLUG);
		$queryGetMeta		= $this->db->get_results($strQuery);
		
		return $queryGetMeta;
	}

	/*	
	==========================================================================
	AJAX METHODS
	==========================================================================	
	*/
	public function get_youtube_details() {
		$arrResults				= array('success' => false);
		
		if ((isset($_REQUEST['id'])) && (strlen($_REQUEST['id']))) {
			$strVideoID			= $_REQUEST['id'];
	
			if (strlen($strVideoID)) {
				$objCURL			= curl_init();
				
				curl_setopt($objCURL,	CURLOPT_URL,			sprintf('http://gdata.youtube.com/feeds/api/videos/%s?v=2&prettyprint=true&alt=jsonc', $strVideoID));
				curl_setopt($objCURL,	CURLOPT_HEADER,			0);
				curl_setopt($objCURL,	CURLOPT_RETURNTRANSFER,	1);
				
				$txtData			= curl_exec($objCURL);
	
				if (!curl_errno($objCURL)) {
					$arrResults['success']	= true;
					$arrResults['data']		= json_decode($txtData);
				} 
				
				curl_close($objCURL);
			}
		}
	
		header('Content-Type: application/json');
		print json_encode($arrResults);
		die();
	}
	
	public function handle_file_upload() {
		$arrUploadPath				= wp_upload_dir();
		$strUploadPath				= $arrUploadPath['basedir'] . DIRECTORY_SEPARATOR . '/';
		$boolCleanupTargetDir		= true; // Remove old files
		$intMaxFileAge				= 5 * 3600; // Temp file age in seconds
		
		ini_set('display_errors', 'on');
		error_reporting(E_ALL);
		
		// 5 minutes execution time
		@set_time_limit(5 * 60);
		
		// Get parameters
		$intCurChunk				= isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$intNumChunks				= isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$strFilename				= isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';
		
		// Clean the fileName for security reasons
		$strFilename				= preg_replace('/[^\w\._]+/', '_', $strFilename);
		
		// Make sure the fileName is unique but only if chunking is disabled
		if ($intNumChunks < 2 && file_exists($strUploadPath . DIRECTORY_SEPARATOR . $strFilename)) {
			$strFilenameExt				= strrpos($strFilename, '.');
			$strFilename_A				= substr($strFilename, 0, $strFilenameExt);
			$strFilename_B				= substr($strFilename, $strFilenameExt);
			$intCounter					= 1;
			
			while (file_exists($strUploadPath . DIRECTORY_SEPARATOR . $strFilename_A . '_' . $intCounter . $strFilename_B))
				$intCounter++;
		
			$strFilename					= $strFilename_A . '_' . $intCounter . $strFilename_B;
		}
		
		$strFilePath				= $strUploadPath . DIRECTORY_SEPARATOR . $strFilename;
		
		// Create target dir
		if (!file_exists($strUploadPath)) {
			mkdir($strUploadPath);
		}
		
		// Remove old temp files	
		if ($boolCleanupTargetDir && is_dir($strUploadPath) && ($strCleanupTempDir = opendir($strUploadPath))) {
			while (($strCleanupTempFile = readdir($strCleanupTempDir)) !== false) {
				$strTempFilePath = $strUploadPath . DIRECTORY_SEPARATOR . $strCleanupTempFile;
		
				// Remove temp file if it is older than the max age and is not the current file
				if (preg_match('/\.part$/', $strCleanupTempFile) && (filemtime($strTempFilePath) < time() - $intMaxFileAge) && ($strTempFilePath != "{$strFilePath}.part")) {
					@unlink($strTempFilePath);
				}
			}
		
			closedir($strCleanupTempDir);
		} else {
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
		}
		
		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"])) {
			$contentType = $_SERVER["HTTP_CONTENT_TYPE"];
		}
		
		if (isset($_SERVER["CONTENT_TYPE"])) {
			$contentType = $_SERVER["CONTENT_TYPE"];
		}
		
		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$objHandleOutput		= fopen("{$strFilePath}.part", $intCurChunk == 0 ? "wb" : "ab");
				
				if ($objHandleOutput) {
					// Read binary input stream and append it to temp file
					$objHandleInput			= fopen($_FILES['file']['tmp_name'], "rb");
		
					if ($objHandleInput) {
						while ($binBufferData = fread($objHandleInput, 4096)) {
							fwrite($objHandleOutput, $binBufferData);
						}
					} else {
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					}
					
					fclose($objHandleInput);
					fclose($objHandleOutput);
					
					@unlink($_FILES['file']['tmp_name']);
				} else {
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream (' . $strFilePath . ')."}, "id" : "id"}');
				}
			} else {
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
			}
		} else {
			// Open temp file
			$objHandleOutput		= fopen("{$strFilePath}.part", $intCurChunk == 0 ? "wb" : "ab");
			
			if ($objHandleOutput) {
				// Read binary input stream and append it to temp file
				$objHandleInput = fopen("php://input", "rb");
		
				if ($objHandleInput) {
					while ($binBufferData = fread($objHandleInput, 4096)) {
						fwrite($objHandleOutput, $binBufferData);
					}
				} else {
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				}
		
				fclose($objHandleInput);
				fclose($objHandleOutput);
			} else {
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			}
		}
		
		// Check if file has been uploaded
		if (!$intNumChunks || $intCurChunk == $intNumChunks - 1) {
			// Strip the temp .part suffix off 
			rename("{$strFilePath}.part", $strFilePath);
			
			// Resize photo
			$objImage		= wp_get_image_editor($strFilePath);
			
			if (!is_wp_error($objImage)) {
				$objImage->resize(225, 165, true);
				$objImage->save($strUploadPath . DIRECTORY_SEPARATOR . 'pt.' . strtolower($strFilename));
			}
		}
		
		// Return JSON-RPC response
		die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
	}
	
	public function get_extravote_question() {
		$arrResults				= array('success' => false, 'earned' => false, 'response' => '');
		$intQuestionID			= 0;
		$strQuestion			= '';
		$arrAnswers				= array();
		$strResponseFilename	= 'frontend-modal-extravote.html';
		
		// IP Check
		if ((isset($_SERVER['REMOTE_ADDR'])) && 
			(strlen($_SERVER['REMOTE_ADDR']))) {
			if ($this->has_voted_today($_SERVER['REMOTE_ADDR'])) {
				$strResponseFilename	= 'frontend-modal-extravote-voted-today.html';
			} else {
				$intCurBonusVoteStatus	= $this->get_bonus_question_status($_SERVER['REMOTE_ADDR']);
				
				switch ($intCurBonusVoteStatus) {
					case self::EXTRAVOTE_STATUS_EARNED : 
						// already answered a question today
						$arrResults['earned']	= true;
						$strResponseFilename	= 'frontend-modal-extravote-earned-not-spent.html';
						break;
						
					case self::EXTRAVOTE_STATUS_SPENT : 
						// already answered a question today
						$strResponseFilename	= 'frontend-modal-extravote-blocked.html';
						break;
						
					case -1 : 
					default : 
						// not yet answered a question today, so allowed to get a question today
						$arrResults['success']	= true;
						
						// get a random question
						/*
						$dtVotingStart			= gmmktime(0, 0, 0, 3, 11, 2013) + (3600 * self::VOTING_TIMEZONE);
						$intVotingDay			= ceil(($dtVotingCurrent - $dtVotingStart) / 86400);
						$dtVotingCurrent		= gmmktime(0, 0, 0) + (3600 * self::VOTING_TIMEZONE);
						*/
						$dtCurrentDate			= gmmktime() - (3600 * abs(self::VOTING_TIMEZONE));
						
						$strQuery				= sprintf(	"SELECT ID, post_title FROM %sposts WHERE post_type = '%s' AND post_status = 'publish' AND DATE(post_date) = '%s' LIMIT 1", 
															$this->db->prefix, 
															self::EXTRAVOTE_POSTTYPE_SLUG, 
															gmdate('Y-m-d', $dtCurrentDate));
						$queryGetQuestion		= $this->db->get_results($strQuery);
						
						if (count($queryGetQuestion)) {
							$objRow					= $queryGetQuestion[0];
							
							$intQuestionID			= intval($objRow->ID);
							$strQuestion			= stripslashes($objRow->post_title);
							
							unset($queryGetQuestion);
			
							$strQuery				= sprintf(	"SELECT meta_value FROM %spostmeta WHERE post_id = %d AND LENGTH(meta_value) > 0 AND meta_key IN ('%s', '%s', '%s', '%s') ORDER BY meta_key ASC", 
																$this->db->prefix, 
																intval($intQuestionID), 
																self::EXTRAVOTE_FIELD_ANSWER1, 
																self::EXTRAVOTE_FIELD_ANSWER2, 
																self::EXTRAVOTE_FIELD_ANSWER3, 
																self::EXTRAVOTE_FIELD_ANSWER4);
							$queryGetAnswers		= $this->db->get_results($strQuery);
							
							if (count($queryGetAnswers)) {
								foreach ($queryGetAnswers as $objRow) {
									$strAnswer			= stripslashes($objRow->meta_value);
									
									if (strlen($strAnswer)) {
										array_push($arrAnswers, $strAnswer);
									}
								}
							}
							unset($queryGetAnswers);
						} else {
							$arrResults['success']	= false;
							$strResponseFilename	= 'frontend-modal-extravote-error.html';
						}
						break;
				}
			}

			if (strlen($strResponseFilename)) {
				$objHandle					= fopen(plugin_dir_path(__FILE__) . 'views/' . $strResponseFilename, 'r');
				
				if ($objHandle) {
					$arrResults['response']		= fread($objHandle, filesize(plugin_dir_path(__FILE__) . 'views/' . $strResponseFilename));
					
					if ($arrResults['success']) {
						$txtAnswerList				= '';
						$intAnswerCounter			= 1;
						
						foreach ($arrAnswers as $strAnswer) {
							$txtAnswerList				.= '<li><input type="radio" name="radAnswer" id="radAnswer_' . $intAnswerCounter . '" value="' . $intAnswerCounter . '" /><label for="radAnswer_' . $intAnswerCounter . '">' . $strAnswer . '</label></li>';
							$intAnswerCounter++;
						}
						
						$arrResults['response']		= str_replace('[QUESTION_ID]', $intQuestionID, $arrResults['response']);
						$arrResults['response']		= str_replace('[QUESTION]', $strQuestion, $arrResults['response']);
						$arrResults['response']		= str_replace('[ANSWERS]', $txtAnswerList, $arrResults['response']);
					}
					
					fclose($objHandle);
				}
			}
		}

		header('Content-Type: application/json');
		print json_encode($arrResults);
		die();
	}

	public function verify_extravote_question() {
		$arrResults				= array('success' => false, 'response' => '');
		$strResponseFilename	= 'frontend-modal-extravote-failure.html';
		$intCorrectAnswer		= 0;
		$txtCorrectAnswer		= '';
		
		// IP Check
		if ((isset($_SERVER['REMOTE_ADDR'])) && 
			(strlen($_SERVER['REMOTE_ADDR'])) && 
			(!$this->has_bonus_vote_today($_SERVER['REMOTE_ADDR']))) {
			if ((isset($_POST['intQuestionID'])) && 
				(isset($_POST['intAnswer'])) && 
				(intval($_POST['intAnswer']) > 0) && 
				(intval($_POST['intQuestionID']) > 0)) {
				$intQuestionID			= intval($_POST['intQuestionID']);
				$intAnswer				= intval($_POST['intAnswer']);
				
				$strQuery				= sprintf(	"SELECT meta_key, meta_value FROM %spostmeta WHERE post_id = %d AND meta_key IN ('%s', '%s')", 
													$this->db->prefix, 
													intval($intQuestionID), 
													self::EXTRAVOTE_FIELD_CORRECT, 
													self::EXTRAVOTE_FIELD_DETAILS);
				$queryGetAnswer			= $this->db->get_results($strQuery);
				
				if (count($queryGetAnswer)) {
					foreach ($queryGetAnswer as $objRow) {
						$strCurMetaKey			= strtolower(stripslashes($objRow->meta_key));
						$strCurMetaValue		= stripslashes($objRow->meta_value);
						
						switch ($strCurMetaKey) {
							case self::EXTRAVOTE_FIELD_CORRECT : 
								$intCorrectAnswer		= intval($strCurMetaValue);
								break;
								
							case self::EXTRAVOTE_FIELD_DETAILS : 
								$txtCorrectAnswer		= wpautop($strCurMetaValue);
								break;
						}
					}
					unset($queryGetAnswer);
					
					if (($intCorrectAnswer > 0) && (strlen($txtCorrectAnswer)) && ($intAnswer == $intCorrectAnswer)) {
							$arrResults['success']	= true;
							$strResponseFilename	= 'frontend-modal-extravote-success.html';
					}

					// Log IP so they can't extra vote again today
					// For boolSpent, 0 = earned (correct answer), 1 = not earned (incorrect answer)
					// For boolCorrect, 0 = incorrect answer, 1 = correct answer
					$strQuery				= sprintf(	"INSERT INTO %snmam_entrant_votes_extra (strIPAddress, dtDate, boolSpent, boolCorrect) VALUES ('%s', '%s', %d, %d)", 
														$this->db->prefix, 
														$this->db->escape($_SERVER['REMOTE_ADDR']), 
														$this->db->escape(gmdate('Y-m-d', time() + (3600 * self::VOTING_TIMEZONE))), 
														(($arrResults['success']) ? 0 : 1), 
														(($arrResults['success']) ? 1 : 0));
					$this->db->query($strQuery);
				} else {
					$strResponseFilename	= 'frontend-modal-extravote-error.html';
				}
			} else {
				$strResponseFilename	= 'frontend-modal-extravote-failure.html';
			}
		} else {
			$strResponseFilename	= 'frontend-modal-extravote-blocked.html';
		}
		
		if (strlen($strResponseFilename)) {
			$objHandle					= fopen(plugin_dir_path(__FILE__) . 'views/' . $strResponseFilename, 'r');
			
			if ($objHandle) {
				$arrResults['response']		= fread($objHandle, filesize(plugin_dir_path(__FILE__) . 'views/' . $strResponseFilename));
				$arrResults['response']		= str_replace('[ANSWER]', $txtCorrectAnswer, $arrResults['response']);
				
				fclose($objHandle);
			}
		}
		
		header('Content-Type: application/json');
		print json_encode($arrResults);
		die();
	}

	/*	
	==========================================================================
	MASS NOTIFY METHODS
	==========================================================================	
	*/
	public function notify_published_entrants() {
		// This method can only be invoked manually, and is only used when voting initially goes "live" during a contest period.
		$strQuery			= sprintf(	"	SELECT intEntrantID, strHeroFirstName, strHeroLastName, strHeroEmail, strHeroCity, strHeroState 
											FROM %snmam_entrant_meta 
											WHERE LENGTH(strHeroEmail) > 0 
											AND boolApproved = 1 
											AND intEntrantID IN (SELECT ID FROM %sposts WHERE post_type = '%s' AND post_status = 'publish') 
											AND intEntrantID NOT IN (SELECT post_id FROM %spostmeta WHERE meta_key = '%s')", 
										$this->db->prefix, 
										$this->db->prefix, 
										self::ENTRANT_POSTTYPE_SLUG, 
										$this->db->prefix, 
										self::ENTRANT_META_NOTIFY_PUB);
		$queryGetEntrants	= $this->db->get_results($strQuery);
		
		if (count($queryGetEntrants)) {
			// send 'entry live' e-mail
			$strQuery			= sprintf(	"SELECT post_title, post_content FROM %sposts WHERE ID = (SELECT post_id FROM %spostmeta WHERE meta_key = 'email_template_type' AND meta_value = 'published') LIMIT 1", 
											$this->db->prefix, 
											$this->db->prefix);
			$queryGetEmail		= $this->db->get_results($strQuery);
			
			if (count($queryGetEmail)) {
				echo count($queryGetEntrants) . ' entrants need a published e-mail sent. Beginning...<br />';
				
				$objRow				= $queryGetEmail[0];
				
				$arrMailHeaders		= array('From: NMEDA.com Mobility Awareness Month <info@mobilityawarenessmonth.com>');
				$strMailSubject		= stripslashes($objRow->post_title);
				$txtMailMessage		= wpautop(stripslashes($objRow->post_content));
				
				foreach ($queryGetEntrants as $objRow) {
					$intCurEntrantID		= intval($objRow->intEntrantID);
					$strCurEntryURL			= get_permalink($intCurEntrantID);
					$strCurHeroEmail		= stripslashes($objRow->strHeroEmail);
					$strCurHeroFirstName	= stripslashes($objRow->strHeroFirstName);
					$strCurHeroLastName		= stripslashes($objRow->strHeroLastName);
					$strCurHeroCity			= stripslashes($objRow->strHeroCity);
					$strCurHeroState		= stripslashes($objRow->strHeroState);
					$txtCurMailMessage		= $txtMailMessage;
					$txtCurMailMessage		= str_replace('[LINK]', '<a href="' . $strCurEntryURL . '">' . $strCurEntryURL . '</a>', $txtCurMailMessage);
					
					if (filter_var($strCurHeroEmail, FILTER_VALIDATE_EMAIL)) {
						wp_mail(	$strCurHeroEmail, 
									$strMailSubject, 
									$txtCurMailMessage, 
									$arrMailHeaders);
						sleep(1);
					}
			
					// update post meta
					update_post_meta($intCurEntrantID, self::ENTRANT_META_HERO_FIRST, $strCurHeroFirstName);
					update_post_meta($intCurEntrantID, self::ENTRANT_META_HERO_LAST, $strCurHeroLastName);
					update_post_meta($intCurEntrantID, self::ENTRANT_META_HERO_CITY, $strCurHeroCity);
					update_post_meta($intCurEntrantID, self::ENTRANT_META_HERO_STATE, $strCurHeroState);

					// mark e-mail as sent
					update_post_meta($intCurEntrantID, self::ENTRANT_META_NOTIFY_PUB, '1');
				}
			}
			unset($queryGetEmail);
		}
	}
}

global $objNMAM;
$objNMAM = new NMEDA_MAM();

?>