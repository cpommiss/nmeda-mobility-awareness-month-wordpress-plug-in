<?php

class NMEDA_MAM_DealerInfo {
	const MODULE_SLUG		= 'nmeda-mam-dealer-info';
	const MODULE_TITLE		= 'Dealer Information';

	private $db;
	
	function __construct() {
		global $wpdb;
		
		$this->db			= &$wpdb;
	}

	public function index() {
		$strQuery			= sprintf(	"SELECT * FROM %snmam_dealers ORDER BY strState ASC, strCity ASC, strName ASC", 
										$this->db->prefix);
		$queryGetDealers	= $this->db->get_results($strQuery);
		
		include dirname(__FILE__) . '/../views/admin-dealers-list.phtml';
	}

	public function edit() {
		if ((isset($_REQUEST['cid'])) && (intval($_REQUEST['cid']) > 0)) {
			$intCurDealerID			= intval($_REQUEST['cid']);
			
			$strQuery				= sprintf(	"SELECT * FROM %snmam_dealers WHERE intID = %d LIMIT 1", 
												$this->db->prefix, 
												intval($intCurDealerID));
			$queryGetDealer			= $this->db->get_results($strQuery);
			
			if (count($queryGetDealer)) {
				$objRow					= $queryGetDealer[0];
				
				$intCurDealerID				= intval($objRow->intID);
				$strCurDealerCode			= stripslashes($objRow->strCode);
				$strCurDealerName			= stripslashes($objRow->strName);
				$strCurDealerContactFirst	= stripslashes($objRow->strContactFirstName);
				$strCurDealerContactLast	= stripslashes($objRow->strContactLastName);
				$strCurDealerAddress1		= stripslashes($objRow->strAddress1);
				$strCurDealerAddress2		= stripslashes($objRow->strAddress2);
				$strCurDealerCity			= stripslashes($objRow->strCity);
				$strCurDealerState			= stripslashes($objRow->strState);
				$strCurDealerZIP			= stripslashes($objRow->strZIP);
				$strCurDealerCountry		= stripslashes($objRow->strCountry);
				$strCurDealerEmail			= stripslashes($objRow->strEmail);
				$strCurDealerPhone			= stripslashes($objRow->strPhone);
				$strCurDealerURL			= stripslashes($objRow->strURL);
				
				include dirname(__FILE__) . '/../views/admin-dealers-edit.phtml';
			} else {
				$this->index();
			}
		} else {
			$this->index();
		}
	}
	
	public function add() {
		include dirname(__FILE__) . '/../views/admin-dealers-add.phtml';
	}

	public function commit() {
		if ((isset($_POST['cid'])) && 
			(isset($_POST['strCode'])) && 
			(isset($_POST['strName'])) && 
			(isset($_POST['strContactFirstName'])) && 
			(isset($_POST['strContactLastName'])) && 
			(isset($_POST['strAddress1'])) && 
			(isset($_POST['strAddress2'])) && 
			(isset($_POST['strCity'])) && 
			(isset($_POST['strState'])) && 
			(isset($_POST['strZIP'])) && 
			(isset($_POST['strCountry'])) && 
			(isset($_POST['strEmail'])) && 
			(isset($_POST['strPhone'])) && 
			(isset($_POST['strURL'])) && 
			(intval($_POST['cid']) > 0) && 
			(strlen($_POST['strCode'])) && 
			(strlen($_POST['strName'])) && 
			(strlen($_POST['strContactFirstName'])) && 
			(strlen($_POST['strContactLastName'])) && 
			(strlen($_POST['strAddress1'])) && 
			(strlen($_POST['strCity'])) && 
			(strlen($_POST['strState'])) && 
			(strlen($_POST['strZIP'])) && 
			(strlen($_POST['strCountry']))) {
			$intCurID					= intval($_POST['cid']);
			$strCurDealerCode			= stripslashes($_POST['strCode']);
			$strCurDealerName			= stripslashes($_POST['strName']);
			$strCurDealerContactFirst	= stripslashes($_POST['strContactFirstName']);
			$strCurDealerContactLast	= stripslashes($_POST['strContactLastName']);
			$strCurDealerAddress1		= stripslashes($_POST['strAddress1']);
			$strCurDealerAddress2		= stripslashes($_POST['strAddress2']);
			$strCurDealerCity			= stripslashes($_POST['strCity']);
			$strCurDealerState			= stripslashes($_POST['strState']);
			$strCurDealerZIP			= stripslashes($_POST['strZIP']);
			$strCurDealerCountry		= stripslashes($_POST['strCountry']);
			$strCurDealerEmail			= stripslashes($_POST['strEmail']);
			$strCurDealerPhone			= stripslashes($_POST['strPhone']);
			$strCurDealerURL			= stripslashes($_POST['strURL']);
			
			$strQuery				= sprintf(	"	UPDATE 	%snmam_dealers 
													SET		strCode				= '%s', 
															strName				= '%s', 
															strContactFirstName	= '%s', 
															strContactLastName	= '%s', 
															strAddress1			= '%s', 
															strAddress2			= '%s', 
															strCity				= '%s', 
															strState			= '%s', 
															strZIP				= '%s', 
															strCountry			= '%s', 
															strEmail			= '%s', 
															strPhone			= '%s', 
															strURL				= '%s' 
													WHERE	intID				= %d", 
												$this->db->prefix, 
												$this->db->escape($strCurDealerCode), 
												$this->db->escape($strCurDealerName), 
												$this->db->escape($strCurDealerContactFirst), 
												$this->db->escape($strCurDealerContactLast), 
												$this->db->escape($strCurDealerAddress1), 
												$this->db->escape($strCurDealerAddress2), 
												$this->db->escape($strCurDealerCity), 
												$this->db->escape($strCurDealerState), 
												$this->db->escape($strCurDealerZIP), 
												$this->db->escape($strCurDealerCountry), 
												$this->db->escape($strCurDealerEmail), 
												$this->db->escape($strCurDealerPhone), 
												$this->db->escape($strCurDealerURL), 
												intval($intCurID));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
	
	public function insert() {
		// common datasets
		if ((isset($_POST['strCode'])) && 
			(isset($_POST['strName'])) && 
			(isset($_POST['strContactFirstName'])) && 
			(isset($_POST['strContactLastName'])) && 
			(isset($_POST['strAddress1'])) && 
			(isset($_POST['strAddress2'])) && 
			(isset($_POST['strCity'])) && 
			(isset($_POST['strState'])) && 
			(isset($_POST['strZIP'])) && 
			(isset($_POST['strCountry'])) && 
			(isset($_POST['strEmail'])) && 
			(isset($_POST['strPhone'])) && 
			(isset($_POST['strURL'])) && 
			(strlen($_POST['strCode'])) && 
			(strlen($_POST['strName'])) && 
			(strlen($_POST['strContactFirstName'])) && 
			(strlen($_POST['strContactLastName'])) && 
			(strlen($_POST['strAddress1'])) && 
			(strlen($_POST['strCity'])) && 
			(strlen($_POST['strState'])) && 
			(strlen($_POST['strZIP'])) && 
			(strlen($_POST['strCountry']))) {
			$strCurDealerCode			= stripslashes($_POST['strCode']);
			$strCurDealerName			= stripslashes($_POST['strName']);
			$strCurDealerContactFirst	= stripslashes($_POST['strContactFirstName']);
			$strCurDealerContactLast	= stripslashes($_POST['strContactLastName']);
			$strCurDealerAddress1		= stripslashes($_POST['strAddress1']);
			$strCurDealerAddress2		= stripslashes($_POST['strAddress2']);
			$strCurDealerCity			= stripslashes($_POST['strCity']);
			$strCurDealerState			= stripslashes($_POST['strState']);
			$strCurDealerZIP			= stripslashes($_POST['strZIP']);
			$strCurDealerCountry		= stripslashes($_POST['strCountry']);
			$strCurDealerEmail			= stripslashes($_POST['strEmail']);
			$strCurDealerPhone			= stripslashes($_POST['strPhone']);
			$strCurDealerURL			= stripslashes($_POST['strURL']);
			
			$strQuery					= sprintf(	"INSERT INTO	%snmam_dealers	(strCode, strName, strContactFirstName, strContactLastName, strAddress1, strAddress2, strCity, strState, strZIP, strCountry, strEmail, strPhone, strURL) 
																	VALUES			('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", 
													$this->db->prefix, 
													$this->db->escape($strCurDealerCode), 
													$this->db->escape($strCurDealerName), 
													$this->db->escape($strCurDealerContactFirst), 
													$this->db->escape($strCurDealerContactLast), 
													$this->db->escape($strCurDealerAddress1), 
													$this->db->escape($strCurDealerAddress2), 
													$this->db->escape($strCurDealerCity), 
													$this->db->escape($strCurDealerState), 
													$this->db->escape($strCurDealerZIP), 
													$this->db->escape($strCurDealerCountry), 
													$this->db->escape($strCurDealerEmail), 
													$this->db->escape($strCurDealerPhone), 
													$this->db->escape($strCurDealerURL));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
	
	public function remove() {
		if ((isset($_REQUEST['cid'])) && 
			(intval($_REQUEST['cid']) > 0)) {
			$intCurID			= intval($_REQUEST['cid']);
			
			$strQuery			= sprintf(	"DELETE FROM %snmam_dealers WHERE intID = %d", 
											$this->db->prefix, 
											intval($intCurID));
			$this->db->query($strQuery);
		}
		
		$this->index();
	}
}

?>